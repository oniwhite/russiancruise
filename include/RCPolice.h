#ifndef _RC_POLICE_H
#define _RC_POLICE_H

#include "RCBaseClass.h"
#include "RCMessage.h"
#include "RCBank.h"
#include "RCDrivingLicense.h"
#include "RCStreet.h"
#include "RCEnergy.h"
#include "RCLight.h"

#define MAX_FINES 100
#define COP_WORK_TIME 1800

struct fine
{
    int     id;
    char    NameRu[64];
    char    NameEn[64];
    int     cash;
};

struct user_fine
{
    int     fine_id;
    int     fine_date;
    string	CopUName;
    string	CopPName;
};

struct RadarObj
{
    short   X;
    short   Y;
    short   Z;
    bool    On = false;
};
struct BarierObj
{
    short X;
    short Y;
    short Z;
    byte  UCID; // cop UCID
    bool  canAdd = false;
    bool  added = false;
};

struct PoliceStat
{
    int DateActive;
    int CurrentDay;
    // Day Stat
    int ArrestWithFineByDay;
    int ArrestWithOutFineByDay;
    int HelpArrestByDay;
    int SolvedIncedentsByDay;
    int FinedByDay;
    int CanceledFinesByDay;
    // All Stat
    int ArrestWithFine;
    int ArrestWithOutFine;
    int HelpArrest;
    int SolvedIncedents;
    int Fined;
    int CanceledFines;
};

struct PolicePlayer: public GlobalPlayer
{
    /** GENERAL **/
    byte	Rank = 0;				// ������: 0 - �����, 1 - ��. ����., 2 - ����., 3 - ����. ����., 4 - �������

    /** COP **/
    bool        cop = false;
    time_t      StartWork; 	//����� ����������� �� �����
    bool        Sirena;
    struct      PoliceStat  PStat;
    RadarObj    Radar;
    BarierObj   Barier;

    /** ��� **/
    int		DTP;
    int		DTPstatus;
    int		DTPfines;
    int		DoneCount = 0;
    int 	LastDtpTime = 0;
    bool 	blame = false;		//���� � ���
    int		FineC = 0;

    /** other players **/
    int		SirenaDist;
    int    	Pogonya;
    int     StopTime = 0;

    map<int, user_fine> fines;

    int 	ThisFineCount;
    char 	ThisFine[20][200];

    int     WorkTime;
    int 	speed_over;
};

class RCPolice: public RCBaseClass
{
private:
    RCPolice();
    ~RCPolice();
    static RCPolice* self;

    RCMessage   *msg;
    RCBank      *bank;
    RCDL        *dl;
    RCStreet    *street;
    RCEnergy	*nrg;
    RCLight		*lgh;

    string siren = "";
    string CopUname = "";

    bool SwitchSirena = false;

    map <byte, PolicePlayer> players; 			// ��� ������
    map <string, time_t> ArestPlayers;			// ������������ ������

    int DTPvyzov[3][32];	// 1 - UCID player, 2 - time, 3 - UCID cop
    int FineAllow[5][MAX_FINES];

    void InsimBTC( struct IS_BTC* packet );
    void InsimBTT( struct IS_BTT* packet );
    void InsimCNL( struct IS_CNL* packet );     // ����� ���� � �������
    void InsimCPR( struct IS_CPR* packet );     // ����� ��������������
    void InsimMCI( struct IS_MCI* packet );
    void InsimMSO( struct IS_MSO* packet );     // ����� �������� ���������
    void InsimNCN( struct IS_NCN* packet );     // ����� ����� ����� �� ������
    void InsimNPL( struct IS_NPL* packet );     // ����� ����� �� ������
    void InsimOBH( struct IS_OBH* packet );
    void InsimPEN( struct IS_PEN* packet );
    void InsimPLA( struct IS_PLA* packet );
    void InsimPLL( struct IS_PLL* packet );     // ����� ���� � �������
    void InsimPLP( struct IS_PLP* packet );     // ����� ���� � �����

    void ReadUserFines( byte UCID );
    void BtnPogonya( byte UCID );
    void ShowFinesPanel( byte UCID, byte UCID2 );
    void CopPayRoll(byte UCID, bool FullWork);
    void ReadFines();
    bool isValidPrefix(xString PName);

    void RadarOn(byte UCID);
    void RadarOff(byte UCID);

    void AddBarier(byte UCID, byte copUCID);
    void RemoveBarier(byte UCID);

    //Stat
    bool LoadCopStat(byte UCID);
    void SaveCopStat(byte UCID);

public:
    static RCPolice* getInstance();

    void ReadConfig(const char* Track);

    //struct fine fines[MAX_FINES];
    Json::Value jFines;

    int init(const char* Dir);

    void Save( byte UCID );
    void SaveAll();
    void SendMTCToCop(string Msg, int Rank, ...);

    void SetSirenLight( string sirenWord );
    bool IsCop(byte UCID);
    int GetCopRank(byte UCID);
    int GetCopDTPstatus(byte UCID);
    int InPursuite(byte UCID);
    int GetFineCount();
    int GetCopCount();
    string GetFineName(byte UCID, int FineID);
    int   GetFineCash(int FineID);
    bool IsPursuit(byte UCID);

    bool isArested(byte UCID);

    void Event();

};
#endif
