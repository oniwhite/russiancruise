#ifndef CLOG_H
#define CLOG_H

#include <DBMySQL.h>

class CLog
{
    public:
        /** @brief Log message into the database
         *
         * @param class or module name
         * @param error, warning, info
         * @return true if log added into the db
         *
         */

        static bool log(string className, string type, string message);
    protected:
    private:
        /** Default constructor */
        CLog();
};

#endif // CLOG_H
