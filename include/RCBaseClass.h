using namespace std;
#ifndef RCBASECLASS_H
#define RCBASECLASS_H

#include "CInsim.h"

#include <CarInfo.h>

#include "RCButtonClickID.h"

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif // M_PI

#include <math.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <ctime>

// c++
#include <exception>
#include <list>
#include <map>
#include <queue>
#include <vector>

#include <sys/stat.h>

#include <DBMySQL.h>
#include <COptions.h>
#include <CLog.h>

#include <json/json.h>

#include "xString.h"

#include "tools.h"
#ifdef __linux__
#define MAX_PATH 4096
#endif // __linux__

typedef queue< ObjectInfo > ObjectsInfo;

typedef map< byte, byte > PLIDMap;

struct GlobalPlayer
{
    CarInfo Info;
    string	UName;              // Username
    string	PName;              // Player name
    string	CName;             // Car Name
    byte    Admin;             // is Admin?

    bool Loaded = false;
    bool onTrack = false;
};

class RCBaseClass
{
public:
    RCBaseClass();
    ~RCBaseClass();

    string ClassName = "RCBaseClass";

    void		 next_packet();
    void         upd_next_packet();
	virtual void SaveAll(){};
	virtual void Save(byte UCID){};
    virtual void Event(){};
    virtual int init(const char* Dir){return 1;};

    void ButtonInfo (byte UCID, const char* Message);
    void ButtonInfo (byte UCID, string Message);
    void ClearButtonInfo (byte UCID);
    void ShowPanel(byte UCID, string Caption, list<string>Text);

    static bool CheckPosition(int polySides,int polyX[],int polyY[],float x,float y);
    static bool CheckPosition(Json::Value polyX, Json::Value polyY,float x,float y);
    static int Distance (int X, int Y, int X1, int Y1);
    static void CCText(string Text, bool useTime = true);

    static string StripText(string Text);

    static const char* GetReason(byte Reason);
    static string packetType(byte type);

    void ButtonClock( byte UCID , int time = 0);
    void ClearButtonClock(byte UCID);

    bool isAdmin(string username);
    bool isAdmin(string username, string group);

    void AddObjects();
	void DelObjects();
	void AddObject( ObjectInfo *object );
	void DelObject( ObjectInfo *object );

	static inline bool FileExists (const std::string& name)
	{
		struct stat status;
        return ( stat(name.c_str(),&status)==0 );
	}

	string expandCar(const char* car);

protected:
    CInsim      *insim;
    DBMySQL     *db;
    char        RootDir[MAX_PATH];
    PLIDMap     PLIDtoUCID;

    ObjectsInfo addObjects;

    ObjectsInfo delObjects;

    virtual void InsimACR( struct IS_ACR* packet ){}
    virtual void InsimAXI( struct IS_AXI* packet ){}
    virtual void InsimAXM( struct IS_AXM* packet ){if( packet->UCID == 0){AddObjects();DelObjects();}}
    virtual void InsimAXO( struct IS_AXO* packet ){}
    virtual void InsimBFN( struct IS_BFN* packet ){}
    virtual void InsimBTC( struct IS_BTC* packet ){}
    virtual void InsimBTT( struct IS_BTT* packet ){}
    virtual void InsimCCH( struct IS_CCH* packet ){}
    virtual void InsimCIM( struct IS_CIM* packet ){}
    virtual void InsimCNL( struct IS_CNL* packet ){}
    virtual void InsimCON( struct IS_CON* packet ){}
    virtual void InsimCPP( struct IS_CPP* packet ){}
    virtual void InsimCPR( struct IS_CPR* packet ){}
    virtual void InsimCRS( struct IS_CRS* packet ){}
    virtual void InsimCSC( struct IS_CSC* packet ){}
    virtual void InsimFIN( struct IS_FIN* packet ){}
    virtual void InsimFLG( struct IS_FLG* packet ){}
    virtual void InsimHLV( struct IS_HLV* packet ){}
    virtual void InsimIII( struct IS_III* packet ){}
    virtual void InsimISM( struct IS_ISM* packet ){}
    virtual void InsimLAP( struct IS_LAP* packet ){}
    virtual void InsimMCI( struct IS_MCI* packet ){if (!insim) return;}
    virtual void InsimMSO( struct IS_MSO* packet ){}
    virtual void InsimNCN( struct IS_NCN* packet ){}
    virtual void InsimNLP( struct IS_NLP* packet ){}
    virtual void InsimNPL( struct IS_NPL* packet ){}
    virtual void InsimOBH( struct IS_OBH* packet ){}
    virtual void InsimPEN( struct IS_PEN* packet ){}
    virtual void InsimPFL( struct IS_PFL* packet ){}
    virtual void InsimPIT( struct IS_PIT* packet ){}
    virtual void InsimPLA( struct IS_PLA* packet ){}
    virtual void InsimPLL( struct IS_PLL* packet ){}
    virtual void InsimPLP( struct IS_PLP* packet ){}
    virtual void InsimPSF( struct IS_PSF* packet ){}
    virtual void InsimREO( struct IS_REO* packet ){}
    virtual void InsimRES( struct IS_RES* packet ){}
    virtual void InsimRST( struct IS_RST* packet ){ ReadConfig( packet->Track ); }
    virtual void InsimSMALL( struct IS_SMALL* packet ){}
    virtual void InsimSLC( struct IS_SLC* packet ){}
    virtual void InsimSPX( struct IS_SPX* packet ){}
    virtual void InsimSSH( struct IS_SSH* packet ){}
    virtual void InsimSTA( struct IS_STA* packet ){}
    virtual void InsimTINY( struct IS_TINY* packet ){}
    virtual void InsimTOC( struct IS_TOC* packet ){}
    virtual void InsimUCO( struct IS_UCO* packet ){}
    virtual void InsimVER( struct IS_VER* packet ){}
    virtual void InsimVTN( struct IS_VTN* packet ){}
    virtual void InsimNCI( struct IS_NCI* packet ){}

    virtual void ReadConfig(const char *Track){};

	Json::Value 		config;
	Json::Reader 		configReader;
	Json::StyledWriter 	configWriter;

private:

};

#endif // RCBASECLASS_H
