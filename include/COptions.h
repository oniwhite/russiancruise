#ifndef COPTIONS_H
#define COPTIONS_H

using namespace std;

#include <iostream>
#include <fstream>
#include <string>
#include <json/json.h>

#include "CLog.h"

class COptions
{
    public:
        static void setRoot(string path);
        static COptions* getInstance();
        static bool isMember(string module, string paramName);
        static vector<string> getModules();
        static vector<string> getParams(string module);
        static string getType(string module, string paramName);
        static string getString(string module, string paramName);
        static int getInt(string module, string paramName);
        static double getDouble(string module, string paramName);
        static bool Set(string module, string paramName, string value);
        static bool Set(string module, string paramName, int value);
        static bool Set(string module, string paramName, double value);
        static bool Delete(string module, string paramName);
    protected:
    private:
        static COptions *self;
        static string rootPath;
        string filename = "options.json";
        bool readed;
        static Json::Value 		config;
        Json::Reader 		configReader;
        Json::StyledWriter 	configWriter;


        COptions();
        ~COptions();
        void load();
        void save();
};

#endif // COPTIONS_H
