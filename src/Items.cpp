#include "Items.h"

Items* Items::_self = nullptr;

Items*
Items::Get()
{
    if(!_self)
    {
        _self = new Items();
    }
    return _self;
}

Items::Items()
{
    LoadItems();
}

Items::~Items()
{
    bool SaveItems();
}

bool
Items::LoadItems()
{
    this->items.clear();

    DB_ROWS res = DBMySQL::Get()->select({},"items");

    if(res.size() > 0)
    {
        for(DB_ROW row: res)
        {
            Item *it = new Item(row["id"].asInt(), row["name"], row["description"]);
            it->setOwner(row["owner"]);
            it->setCarrier(row["carrier"]);
            it->setUseCount(row["useCount"].asInt());
            it->setExpires(row["expires"].asInt());
            it->setExtInfo(row["ext_info"]);
            it->setPrice(row["price"].asDouble());
            this->items[row["id"].asInt()] = it;
        }
    }
    return true;
}

bool
Items::SaveItems()
{
    for(auto& it: items)
        it.second->Save();

    return true;
}

int
Items::Add(xString name, xString description)
{
    int id = DBMySQL::Get()->insert("items",{{"name",name.toLower()},{"description",description.toLower()}});

    if(id > 0)
    {
        Item *it = new Item(id, name, description);

        this->items[id] = it;
    }

    return id;
}

bool
Items::Delete(int id)
{
    if(items.find(id) == items.end())
        return false;

    if(!DBMySQL::Get()->exec("DELETE FROM items WHERE id = " + ToString(id)))
    {
        return false;
    }

    items.erase(id);
    return true;
}

Item*
Items::GetItem(int id)
{
    return items.at(id);
}

int
Items::GetNearestItem(Vec position, int distance)
{
    int res = 0;
    int minDist = -1;

    for(auto it: items)
    {
        Item* item = it.second;

        if(item->getPos().x == 0 && item->getPos().y == 0)
            continue;

        int dist = RCBaseClass::Distance(position.x, position.y, item->getPos().x, item->getPos().y);

        if(minDist < 0 && dist <= distance)
        {
            minDist = dist;
            res = it.first;
        }

        if(dist < minDist && dist <= distance)
        {
            minDist = dist;
            res = it.first;
        }
    }

    return res;
}

vector<int>
Items::getItemsByOwner(xString owner)
{
    vector<int> res;

    for(auto it: items)
    {
        Item* item = it.second;
        if(item->getOwner() == owner.toLower())
            res.push_back(it.first);
    }

    return res;
}

vector<int>
Items::getItemsByOwner(xString owner, xString name)
{
    vector<int> res;

    for(auto it: items)
    {
        Item* item = it.second;
        if(item->getOwner() == owner.toLower() && item->getName() == name.toLower())
            res.push_back(it.first);
    }

    return res;
}

vector<int>
Items::getItemsByOwner(xString owner, xString name, xString carrier)
{
    vector<int> res;

    for(auto it: items)
    {
        Item* item = it.second;
        if(item->getOwner() == owner.toLower() && item->getName() == name.toLower() && item->getCarrier() == carrier.toLower())
            res.push_back(it.first);
    }

    return res;
}

vector<int>
Items::getItemsByCarrier(xString carrier)
{
    vector<int> res;

    for(auto it: items)
    {
        Item* item = it.second;
        if(item->getCarrier() == carrier.toLower())
            res.push_back(it.first);
    }

    return res;
}

vector<int>
Items::getItemsByCarrier(xString carrier, xString name)
{
    vector<int> res;

    for(auto it: items)
    {
        Item* item = it.second;
        if(item->getCarrier() == carrier.toLower() && item->getName() == name.toLower())
            res.push_back(it.first);
    }

    return res;
}


/***********************************************************************/

Item::Item(int id, string name, string description)
{
    this->id = id;
    this->name = name;
    this->description = description;
    this->owner = "";
    this->carrier = "";
    this->reUsable = "N";
    this->useCount = 1;
    this->expires = 0;
    this->ext_info = "";
    this->price = 0;
}

bool
Item::Save()
{

    DB_ROW data;
    data["name"] = this->name;
    data["description"] = this->description;
    data["owner"] = this->owner;
    data["carrier"] = this->carrier;
    data["reUsable"] = (this->reUsable);
    data["useCount"] = ToString(this->useCount);
    data["expires"] = ToString(this->expires);
    data["ext_info"] = this->ext_info;
    data["price"] = ToString(this->price);
    bool res = DBMySQL::Get()->update("items",data,{{"id", ToString(this->id)}});

    return res;
}

string Item::getName()
{
    return name;
}

string Item::getDescription()
{
    return description;
}

Item*
Item::setOwner(xString owner)
{
    this->owner = owner.toLower();
    return this;
}

string
Item::getOwner()
{
    return owner;
}

Item*
Item::setCarrier(xString carrier)
{
    this->carrier = carrier.toLower();
    return this;
}

string
Item::getCarrier()
{
    return carrier;
}

bool
Item::IsUsable()
{
    if(expires > 0 && expires < time(nullptr))
        return false;

    if(useCount == 0)
        return false;

    return true;
}

bool
Item::Use()
{
    if(this->IsUsable())
    {
        this->useCount --;
        if(this->useCount <= 0 && this->reUsable == "N")
        {
            Items::Get()->Delete( this->id );
        }
        return true;
    }
    return false;
}

Vec
Item::getPos()
{
    return position;
}

Item*
Item::setPos(Vec position)
{
    this->position.x = position.x;
    this->position.y = position.y;
    this->position.z = position.z;
    return this;
}

int
Item::getUseCount()
{
    return useCount;
}

Item*
Item::setUseCount(int useCount)
{
    this->useCount = useCount;
    return this;
}

int
Item::getExpires()
{
    return this->expires;
}

Item*
Item::setExpires(int expiries)
{
    this->expires = expires;
    return this;
}

xString
Item::getExtInfo()
{
    return this->ext_info;
}

Item*
Item::setExtInfo(xString info)
{
    this->ext_info = info;
    return this;
}

double
Item::getPrice()
{
    return this->price;
}

Item*
Item::setPrice(double price)
{
    this->price = price;
    return this;
}
