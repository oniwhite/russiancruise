#include "RCPizza.h"

RCPizza*
RCPizza::self = nullptr;

RCPizza*
RCPizza::getInstance()
{
    if(!self)
        self = new RCPizza();

    return self;
}

RCPizza::RCPizza()
{
    ClassName = "RCPizza";
    memset(&PStore, 0, sizeof(Store));
    memset(&TrackInf, 0, sizeof(pizza_info));
    memset(&zone, 0, sizeof(place));
}

RCPizza::~RCPizza()
{

}

int RCPizza::init(const char* Dir)
{
    strcpy(RootDir,Dir);
    this->db = DBMySQL::Get();
    if (!this->db)
    {
        printf("RCPizza: Can't sctruct MySQL Connector\n");
        return -1;
    }

    insim = CInsim::getInstance();
    if (!insim)
    {
        printf ("RCPizza: Can't struct CInsim class");
        return -1;
    }

    msg = RCMessage::getInstance();
    if (!msg)
    {
        printf ("RCPizza: Can't struct RCMessage class");
        return -1;
    }

    bank = RCBank::getInstance();
    if (!bank)
    {
        printf ("RCPizza: Can't struct RCBank class");
        return -1;
    }

    nrg = RCEnergy::getInstance();
    if (!nrg)
    {
        printf ("RCPizza: Can't struct RCEnergy class");
        return -1;
    }

    dl = RCDL::getInstance();
    if (!dl)
    {
        printf ("RCPizza: Can't struct RCDL class");
        return -1;
    }

    street = RCStreet::getInstance();
    if (!street)
    {
        printf ("Can't struct RCStreet class");
        return -1;
    }

    ginfo_time = time(nullptr)+60;

    CCText("^3"+ClassName+":\t\t^2inited");
    return 0;
}

void RCPizza::Deal(byte UCID)
{

    if (!players[UCID].isWork)
    {
        Json::Value cars = RCCore::getInstance()->getCars();
        int PIZZA_UF1_PRICE = cars["UF1"]["price"].asInt();

        if (this->CarsInWork < this->NumCars)
        {
            players[UCID].isWork = true;
            players[UCID].WorkAccept = 0;
            players[UCID].FreeEat = false;
            insim->SendMTC(UCID, msg->_(UCID, "4000"));
            this->CarsInWork ++;
        }
        else if(CarsInWork >= NumCars && Capital > PIZZA_MIN_CAPITAL + PIZZA_UF1_PRICE)
        {
            this->NumCars++;
            players[UCID].isWork = true;
            players[UCID].WorkAccept = 0;
            players[UCID].FreeEat = false;
            insim->SendMTC(UCID, msg->_(UCID, "4000"));
            this->CarsInWork ++;
        }
        else
        {
            insim->SendMTC(UCID, msg->_(UCID, "4104"));
            return;
        }
    }
    else if (players[UCID].isWork)
    {
        insim->SendMTC(UCID, msg->_(UCID, "4001"));
    }
    else
    {
        insim->SendMTC(UCID, msg->_(UCID, "4002"));
    }
}

void
RCPizza::Undeal( byte UCID ,string Reason = "")
{
    if (players[UCID].WorkAccept == 3)
    {
        ShopAccepted = false;
    }

    ClearButtonClock(UCID);
    ClearButtonInfo(UCID);

    if(Reason.size() > 0)
        insim->SendMTC(UCID, Reason);

    insim->SendMTC(UCID, msg->_(UCID, "4100"));
    players[UCID].isWork = false;
    players[UCID].WorkDestinaion =0;
    players[UCID].WorkDest = "";
    players[UCID].WorkAccept = 0;
    players[UCID].FreeEat = false;
    CarsInWork --;

    insim->SendBFN(UCID, 212);

    if (players[UCID].WorkPlayerAccept != 0)
    {
        players[ players[UCID].WorkPlayerAccept ].Pizza = 1;
    }

    players[UCID].WorkPlayerAccept = 0;
}

void RCPizza::Take(byte UCID)
{
    if (!players[UCID].isWork)
    {
        insim->SendMTC(UCID, msg->_(UCID, "4002"));
        return;
    }

    if (players[UCID].CName != "UF1"  && players[UCID].CName != "D19B9B")
    {
        insim->SendMTC(UCID, msg->_(UCID, "4102"));
        return;
    }

    if (players[UCID].WorkAccept == 1)
    {
        /**
        *	====	������� �����	====
        *
        *   ���� ������ ����� - 1 �� ���� 0.6 �� ����, 0.9 �������, 0.4 ����
        *   ������������� �� ������ = 3+ 3 +40 +83 = 132 ���
        *   ������������� ���� 320 ���
        *   ������ �� ������������ 188 ���
        *   ������ �� �������� 240 ���.
        **/

        PStore.Muka -= 0.25; // 3 rur
        PStore.Voda -= 0.2;    // 3
        PStore.Ovoshi -= 0.5;   //40
        PStore.Cheese -= 0.15; // 83

        if (players[UCID].WorkPlayerAccept == 0) // ���� ������� �����
        {
            srand(time(NULL));
            int place = rand()%zone.NumPoints;

            int worktime = time(nullptr);
            players[UCID].WorkTime = worktime + PIZZA_WORK_TIME;
            players[UCID].WorkDestinaion = place;
            players[UCID].WorkAccept = 2;

            int StreetID = street->GetStreetId(zone.point[place].X, zone.point[place].Y);

            string str = StringFormat(
                msg->_(UCID, "4200"),
                msg->_(UCID, zone.point[place].PlaceName),
                street->GetStreetName(UCID, StreetID)
            );

            insim->SendMTC(UCID, "^3| " + str);

            players[UCID].WorkDest = str;
        }
        else if (players[UCID].WorkPlayerAccept != 0) // ������� �����
        {
            int worktime = time(nullptr);

            players[UCID].WorkTime = worktime + PIZZA_WORK_TIME;
            players[UCID].WorkDestinaion = players[UCID].WorkPlayerAccept;
            players[UCID].WorkAccept = 2;
            char text[128];
            sprintf(text, msg->_(UCID, "4201"), players[players[UCID].WorkPlayerAccept].PName.c_str());
            players[UCID].WorkDest = text+4;
            insim->SendMTC(UCID, text);
        }
    }

    else if (players[UCID].WorkAccept == 2)
        insim->SendMTC(UCID, msg->_(UCID, "4202")); // ������ ������� ���� �����
    else
        insim->SendMTC(UCID, msg->_(UCID, "4203")); // Wait until i call you
}

void RCPizza::Done(byte UCID)
{
    if ((players[UCID].isWork) && (players[UCID].WorkAccept == 2))
    {
        insim->SendMTC(UCID, msg->_(UCID, "4300"));

        players[UCID].WorkPlayerAccept = 0;
        players[UCID].WorkDestinaion = 0;
        players[UCID].WorkDest = "";
        players[UCID].WorkCountDone ++;
        players[UCID].WorkAccept = 0;
        players[UCID].WorkZone = 0;


        if((players[UCID].WorkCountDone % 10) == 0)
        {
            players[UCID].FreeEat = true;
            insim->SendMTC(UCID, msg->_(UCID, "4213"));
        }

        int cash = 50 * abs(1 - (players[UCID].WorkTime - time(nullptr)) / PIZZA_WORK_TIME);
        bank->AddCash(UCID, 248 + cash, true);
        Capital += 420 - cash;

        dl->AddSkill(UCID, 1.0f - (float(dl->GetLVL(UCID))/200.0f));
        ClearButtonInfo(UCID);
        ClearButtonClock(UCID);
    }
}

void RCPizza::ReadConfig(const char *Track)
{
    string file = StringFormat("%s/RCPizza/%s", RootDir, Track);

	msg->ReadLangDir(file);

    file = StringFormat("%s/RCPizza/%s/points.txt", RootDir, Track);

    ifstream readf;
    readf.open(file, ios::in);

    if(!readf.is_open())
    {
        CCText("  ^7RCPizza    ^1ERROR: ^8file " + file + " not found");
        return;
    }
    int point = 0;
    while (readf.good())
    {
        char str[128];
        readf.getline(str, 128);

        if (strlen(str) > 1)
        {
            if (strstr(str, "/dealer") != NULL)
            {
                for (int i=0; i<4; i++)
                {
                    readf.getline(str, 128);
                    char * X;
                    char * Y;
                    X = strtok (str, ", ");
                    Y = strtok (NULL, ", ");
                    zone.dealX[i] = atoi(X);
                    zone.dealY[i] = atoi(Y);
                }
            }

            if (strstr(str, "/shop") != NULL)
            {
                readf.getline(str, 128);
                int count = atoi(str);
                TrackInf.ShopCount = count;

                for (int i=0 ; i<count; i++)
                {
                    readf.getline(str, 128);
                    char * X;
                    char * Y;
                    X = strtok (str, ";");
                    Y = strtok (NULL, ";");
                    TrackInf.XShop[i] = atoi(X);
                    TrackInf.YShop[i] = atoi(Y);
                }
            }

            if (strstr(str, "point") != NULL)
            {
                readf.getline(str, 64);
                strncpy(zone.point[point].PlaceName, str, strlen(str));

                readf.getline(str, 128);

                char * X;
                char * Y;
                X = strtok (str, ", ");
                Y = strtok (NULL, ", ");

                zone.point[point].X = atoi(X);
                zone.point[point].Y = atoi(Y);

                point ++;
                zone.NumPoints = point;
            }
        }
    }
    readf.close();

    /*** READ STORE DATA ***/

    DB_ROWS res = db->select({},"pizza_store");

    if(res.size() > 0)
    {
        DB_ROW row = res.front();

        PStore.Muka = row["Muka"].asFloat();
        PStore.Voda = row["Voda"].asFloat();
        PStore.Ovoshi = row["Ovoshi"].asFloat();
        PStore.Cheese = row["Cheese"].asFloat();

        Capital = row["Capital"].asDouble();
        NumCars = row["NumCars"].asInt();

    }
    else
    {
        db->insert("pizza_store",{
                   {"Muka","0"},
                   {"Voda","0"},
                   {"Ovoshi","0"},
                   {"Capital","1000"},
                   {"NumCars","5"}}
                   );
    }

    CCText("  ^7RCPizza\t^2OK");
}


void RCPizza::InsimNCN(struct IS_NCN* packet)
{
    if(packet->UCID == 0)
        return;

    NumP = packet->Total;
    players[packet->UCID].UName = packet->UName;
    players[packet->UCID].PName = packet->PName;
    players[packet->UCID].Admin = packet->Admin;

    if(!packet->Admin)
    {
        players[packet->UCID].Admin = this->isAdmin(packet->UName);
    }

    DB_ROWS res = db->select({}, "pizza", {{"username",players[packet->UCID].UName}});

	if( res.size() > 0 )
	{
		DB_ROW row = res.front();
		players[packet->UCID].WorkCountDone = row["count"].asInt();
	} else {
        db->insert("pizza", {{"username", players[packet->UCID].UName},{"count", "0"}});
	}
}

void RCPizza::InsimCNL(struct IS_CNL* packet)
{
    if (players[packet->UCID].WorkAccept == 3)
        ShopAccepted = false;

    if (players[packet->UCID].isWork)
        CarsInWork--;

    if (players[packet->UCID].Pizza == 1)
	{
        for (auto p: players)
		{
            if (players[p.first].WorkPlayerAccept == packet->UCID)
            {
                players[p.first].WorkPlayerAccept = 0;
                break;
            }
		}
	}

	if (players[packet->UCID].WorkPlayerAccept != 0)
    {
        players[ players[packet->UCID].WorkPlayerAccept ].Pizza = 1;
    }

    Save(packet->UCID);
    NumP = packet->Total;

    players.erase(packet->UCID);
}

void RCPizza::InsimNPL(struct IS_NPL* packet)
{
    PLIDtoUCID[packet->PLID] = packet->UCID;
    players[packet->UCID].CName = expandCar(packet->CName);
}

void RCPizza::InsimPLP(struct IS_PLP* packet)
{
}

void RCPizza::InsimPLL(struct IS_PLL* packet)
{
    PLIDtoUCID.erase(packet->PLID);
}

void RCPizza::InsimCPR(struct IS_CPR* packet)
{
    players[packet->UCID].PName = packet->PName;
    if (( players[packet->UCID].PName.find(PIZZA_NICK_PIZZA) == string::npos ||
          players[packet->UCID].PName.find(PIZZA_NICK_TOFU) == string::npos ) &&
          players[packet->UCID].isWork)
    {
        Undeal(packet->UCID, msg->_(packet->UCID, "4103"));
    }
}
void RCPizza::InsimMCI (struct IS_MCI* pack_mci)
{
    for (int i = 0; i < pack_mci->NumC; i++)
    {
        byte UCID = PLIDtoUCID[pack_mci->Info[i].PLID];

        int X = pack_mci->Info[i].X / 65536;
        int Y = pack_mci->Info[i].Y / 65536;
        int S = (int)pack_mci->Info[i].Speed * 360 / 32768;

        // ���� �������� ����
        if (CheckPosition(4, zone.dealX, zone.dealY, X, Y))
        {
            if (!players[UCID].Zone)
            {
                players[UCID].Zone = true;
                insim->SendMTC(UCID, msg->_(UCID, "1600")); // pizza u Jony

                if (players[UCID].FreeEat)
					insim->SendMTC(UCID, msg->_(UCID, "FreeEat")); // omnomnom

                if (players[UCID].isWork == false)
                    insim->SendMTC(UCID, msg->_(UCID, "1601")); // deal
                else
                    insim->SendMTC(UCID, msg->_(UCID, "1602")); // undeal

                if (players[UCID].FreeEat)
					insim->SendMTC(UCID, msg->_(UCID, "FreeEat")); // undeal

                // ������ ����� �� ����
                if (players[UCID].WorkAccept != 0)
                    Take(UCID);
            }
        }
        else
            players[UCID].Zone = false;

        // �������� ���� ���� ����� � ���� �������� � �������� ����� ����
        /** Zones (PitSave, shop, etc) **/
        if (Distance(X, Y, zone.point[players[UCID].WorkDestinaion].X, zone.point[players[UCID].WorkDestinaion].Y) < 10 && S < 5)
        {
            Done(UCID);
        }

        byte PL_UCID = players[UCID].WorkPlayerAccept;

        if (PL_UCID != 0 && players[UCID].WorkAccept == 2 && S < 5 && (players[PL_UCID].Info.Speed * 360 / 32768) < 5)
        {
            int PL_X = players[PL_UCID].Info.X / 65536;
            int PL_Y = players[PL_UCID].Info.Y / 65536;

            if (Distance(X, Y, PL_X, PL_Y) < 10) // ���� ��������� �� ������ ������ 10 ������
            {
                Done(UCID);
                bank->RemCash(PL_UCID, 800);

                float energy = nrg->GetEnergy(PL_UCID);
                if(energy < 100)
                {
                    float toAdd = 80;

                    if(energy + toAdd > 100)
                        toAdd = 100 - energy;

                    nrg->AddEnergy(PL_UCID,toAdd);
                }

                insim->SendMTC(PL_UCID, msg->_(PL_UCID, "1604"));
                players[PL_UCID].Pizza = 0;
            }
        }

        if (players[UCID].WorkAccept == 3 && RCCore::getInstance()->inShop(UCID)) // ���� ������ ������� �������� �������� � �� ������� � �������
        {
            players[UCID].WorkDestinaion =0;
            players[UCID].WorkAccept = 0;
            players[UCID].WorkPlayerAccept = 0;
            //insim->SendBFN(UCID, 212);
            insim->SendMTC(UCID, msg->_(UCID, "4205"));

            // ��������� ����� �� ��������
            if (PStore.Muka < 10)
            {
                PStore.Muka += 99;
                Capital -= 99 * 12;
                bank->AddToBank(99 * 12);
            }

            if (PStore.Voda < 10)
            {
                PStore.Voda += 99;
                Capital -= 99 * 10;
                bank->AddToBank(99 * 10);
            }

            if (PStore.Ovoshi < 10)
            {
                PStore.Ovoshi += 99;
                Capital -= 99 * 80;
                bank->AddToBank(99 * 80);
            }

            if (PStore.Cheese < 10)
            {
                PStore.Cheese += 99;
                Capital -= 99 * 560;
                bank->AddToBank(99 * 560);
            }

            ShopAccepted = false;
        }

        players[UCID].Info = pack_mci->Info[i];
    }
}

void RCPizza::InsimMSO(struct IS_MSO* packet)
{
    if (packet->UCID == 0)
        return;

    string Message = packet->Msg + packet->TextStart;

    if (Message == "!pstat")
    {
        string Text;
        Text = StringFormat(msg->_(packet->UCID, "4206"), Capital);
        insim->SendMTC(packet->UCID, Text);
        Text = StringFormat(msg->_(packet->UCID, "4207"), PStore.Voda, PStore.Muka, PStore.Ovoshi, PStore.Cheese);
        insim->SendMTC(packet->UCID, Text);

        if (CarsInWork==0)
        {
            insim->SendMTC(packet->UCID, msg->_(packet->UCID, "4208"));
        }
        else
        {
            Text = StringFormat(msg->_(packet->UCID, "4209"), CarsInWork, NumCars);
            insim->SendMTC(packet->UCID, Text);

            for (auto p: players)
			{
                if (players[p.first].isWork)
                {
                    Text = StringFormat(msg->_(packet->UCID, "4210"), players[p.first].PName.c_str(), players[p.first].WorkDest.c_str(), players[p.first].WorkCountDone);
                    insim->SendMTC(packet->UCID, Text);
                }
			}
        }
    }

    if (Message == "!eat" && players[packet->UCID].Zone == 1 && players[packet->UCID].isWork)
    {
        if (players[packet->UCID].FreeEat)
        {
            if(nrg->GetEnergy(packet->UCID) + 80 > 100)
            {
                nrg->AddEnergy(packet->UCID, 80 - nrg->GetEnergy(packet->UCID));
            }
            else
                nrg->AddEnergy(packet->UCID, 80);

            insim->SendMTC(packet->UCID, msg->_(packet->UCID, "4211"));
            players[packet->UCID].FreeEat = false;
        }
        else
            insim->SendMTC(packet->UCID, msg->_(packet->UCID, "4212"));
    }

    if (Message == "!deal")
	{
        if (!players[packet->UCID].Zone || players[packet->UCID].isWork)
            return;

        if (players[packet->UCID].CName != "UF1" && players[packet->UCID].CName != "D19B9B")
        {
            insim->SendMTC(packet->UCID, msg->_(packet->UCID, "4102"));
            return;
        }


        if ( players[packet->UCID].PName.find(PIZZA_NICK_PIZZA) == string::npos &&
             players[packet->UCID].PName.find(PIZZA_NICK_TOFU) == string::npos )
        {
            insim->SendMTC(packet->UCID, msg->_(packet->UCID, "4103"));
            return;
        }

        Deal(packet->UCID);
	}

    if (Message == "!undeal")
    {
        if (players[packet->UCID].Zone && players[packet->UCID].isWork)
        {
            Undeal(packet->UCID);
        }
    }

    //!pizza
    if (Message == "!tofu" || Message == "!pizza")
    {
        if (nrg->GetEnergy(packet->UCID) > 80)
        {
            insim->SendMTC(packet->UCID, msg->_(packet->UCID, "NotLesEn"));
            return;
        }

        if (bank->GetCash( packet->UCID ) < 800)
        {
            insim->SendMTC(packet->UCID, msg->_( packet->UCID, "2001" ));
            return;
        }

        if (players[packet->UCID].isWork)
        {
            insim->SendMTC(packet->UCID, msg->_(packet->UCID, "NoTofuWhWork"));
            return;
        }

        if (CarsInWork == 0)
        {
            insim->SendMTC(packet->UCID, msg->_(packet->UCID, "NoTofuCars"));
            return;
        }

        if (players[packet->UCID].Pizza == 0)
        {
            players[packet->UCID].Pizza = 1;
            insim->SendMTC(packet->UCID, msg->_(packet->UCID, "DelTofu")); // ����� ������
        }
        else
            insim->SendMTC(packet->UCID, msg->_(packet->UCID, "UAreDel")); //�� ��� �������
    }

    //!test
    if (Message == "!test" && players.at(packet->UCID).Admin)
    {
        for (int i=0;i<zone.NumPoints;i++)
        {
            char str[64];

            int StreetID = street->GetStreetId(zone.point[i].X, zone.point[i].Y);

            sprintf(str, msg->_(packet->UCID, "4200"), msg->_(packet->UCID, zone.point[i].PlaceName), street->GetStreetName(packet->UCID, StreetID));
            insim->SendMTC(packet->UCID, "^3| " +  (string)str);
        }
    }
}

bool RCPizza::IsWork (byte UCID)
{
    return players[UCID].isWork;
}

void RCPizza::Event()
{
    /** ����� ������ � �������� � ������� �� ���� ������ ������ � ���� **/
    for (auto& plit: players)
    {
        if (players[plit.first].WorkAccept != 0)
        {
            int nowtime = time(nullptr);
            if (players[plit.first].WorkTime <= nowtime)
            {
                if (players[plit.first].WorkAccept == 3) // �� ����� �������� ��������
                    ShopAccepted = false;

                if (players[plit.first].WorkAccept != 0)
                {
                    insim->SendMTC(plit.first, msg->_(plit.first, "4101"));
                    players[plit.first].isWork = false;
                    players[plit.first].WorkAccept = 0;

                    if (players[plit.first].WorkPlayerAccept != 0)
					{
                        players[ players[plit.first].WorkPlayerAccept ].Pizza = 0;
					}

                    players[plit.first].WorkPlayerAccept = 0;
                    CarsInWork --;
                    ClearButtonInfo(plit.first);
                    ClearButtonClock(plit.first);
                }
            }
        }

        if (players[plit.first].WorkAccept != 0)
        {
            ButtonClock(plit.first, players[plit.first].WorkTime - time(nullptr));
            ButtonInfo(plit.first, players[plit.first].WorkDest);
        }
    }

    /** ����� ����� ������� **/
    if (ginfo_time <= time(nullptr))
    {
        int pizza_time = 600 / (NumP + 1);
        ginfo_time += pizza_time;

        if ((PStore.Muka > 5) && (PStore.Voda > 5) && (PStore.Ovoshi > 5) && (PStore.Cheese > 5))
        {
            for (auto& plit: players)
            {
                if ((plit.first  !=0) && (players[plit.first].isWork) && (players[plit.first].WorkAccept == 0))
                {
                    /** ������ ������������� �� ������� ������ **/
                    for (auto& plit2: players)
                    {
                        if (plit2.first != plit.first && players[plit2.first].Pizza == 1)
                        {
                            insim->SendMTC(plit.first, msg->_(plit.first, "2201"));
                            players[plit.first].WorkDest = msg->_(plit.first, "2202");
                            players[plit.first].WorkAccept = 1;
                            players[plit.first].WorkPlayerAccept = plit2.first;
                            players[plit.first].WorkZone =0;
                            int worktime = time(nullptr);
                            players[plit.first].WorkTime = worktime+60 * 6;

                            players[plit2.first].Pizza = 2;
                            break; // ����� �������� ������ ������ ������
                        }
                    }
                    break;
                }
            }

            /** ����� ����� ����� **/
            for (auto& plit: players)
            {
                if ((plit.first  !=0) && (players[plit.first].isWork) && (players[plit.first].WorkAccept == 0))
                {
                    insim->SendMTC(plit.first, msg->_(plit.first, "2201"));
                    players[plit.first].WorkDest = msg->_(plit.first, "2202");

                    players[plit.first].WorkAccept =1;
                    players[plit.first].WorkPlayerAccept =0;
                    players[plit.first].WorkZone =0;
                    int worktime = time(nullptr);
                    players[plit.first].WorkTime = worktime+60 * 6;
                    break; // ����� �������� ������ ������ ������
                }
            }
        }
    }

    /** ����� ��������� � �������� **/
    if ((PStore.Muka < 10 || PStore.Voda < 10 || PStore.Ovoshi < 10 || PStore.Cheese < 10) && ShopAccepted == false)
    {
        for (auto& plit: players)
        {
            if (plit.first !=0 && players[plit.first].isWork && players[plit.first].WorkAccept == 0)
            {
                players[plit.first].WorkDest = msg->_(plit.first, "4204");
                insim->SendMTC(plit.first, StringFormat("^3| %s", msg->_(plit.first, "4204")));

                players[plit.first].WorkAccept = 3;
                players[plit.first].WorkPlayerAccept = 0;
                players[plit.first].WorkZone = 0;
                int worktime = time(nullptr);
                players[plit.first].WorkTime = worktime+60 * 6;
                ShopAccepted = true;
                break; // ����� �������� ������ ������ ������
            }
        }
    }
}

void
RCPizza::Save( byte UCID )
{
    /** Save Pizza Info **/
    DB_ROW upd;
    upd["Muka"] = ToString(PStore.Muka);
    upd["Voda"] = ToString(PStore.Voda);
    upd["Ovoshi"] = ToString(PStore.Ovoshi);
    upd["Cheese"] = ToString(PStore.Cheese);
    upd["Capital"] = ToString(Capital);
    upd["NumCars"] = ToString(NumCars);

    db->update("pizza_store", upd, {});

    db->update("pizza",{{"count", ToString(players[UCID].WorkCountDone)}},{{"username", players[UCID].UName}});

}

void
RCPizza::SaveAll()
{
    for( auto i: players)
    {
        Save(i.first);
    }
}
