#include "DBMySQL.h"

DBMySQL::DBMySQL()
{
    connected = false;
}

DBMySQL::DBMySQL(const string host, const int port, const string username, const string password, const string database)
{
    this->host = host;
    this->port = port;
    this->username = username;
    this->password = password;
    this->database = database;
    this->connected = false;
}

DBMySQL::~DBMySQL()
{
    if(connected)
        mysql_close(&dbconn);
}

DBMySQL* DBMySQL::dbInstanse = nullptr;

DBMySQL* DBMySQL::Get()
{
    if(!dbInstanse)
    {
        dbInstanse = new DBMySQL();
    }
    return dbInstanse;
}

DBMySQL* DBMySQL::Get(const string host, const int port, const string username, const string password, const string database)
{
    if(!dbInstanse)
    {
        dbInstanse = new DBMySQL(host, port, username, password, database);
    }
    return dbInstanse;
}

bool
DBMySQL::connect()
{
 	if (!mysql_init(&dbconn))
    {
        cout << "Can't create MySQL-descriptor" << endl;
        return false;
    }
    #ifdef __linux__
    bool reconnect = 1;
    #else
    char reconnect = '1';
    #endif
    mysql_options( &dbconn , MYSQL_OPT_RECONNECT, &reconnect ); // ��������� ���������������

	connected = mysql_real_connect( &dbconn , host.c_str(), username.c_str(), password.c_str(), database.c_str(), port, NULL, 0);

	if(connected)
    {
        columns = collectTables(database);
    }

	return connected;
}

map<string,vector<string>>
DBMySQL::collectTables(string dbName)
{
    //if(!this->connected)
        //throw new logic_error("No connection to DB");

    mtx.lock();
    map<string,vector<string>> out;
    out.clear();

    MYSQL_RES *result = mysql_list_tables(&dbconn, nullptr);

    if(!result)
    {
        cout<< "DB collectTables ERROR: " + getError() << endl;
        mtx.unlock();
        return out;
    }

    MYSQL_ROW row;
    row = mysql_fetch_row(result);
    while(row != nullptr)
    {
        out[ row[0] ] = collectColumns( row[0] );
        row = mysql_fetch_row(result);
    }

    mysql_free_result(result);
    mtx.unlock();
    return out;
}

vector<string>
DBMySQL::collectColumns(string tableName)
{
    //if(!this->connected)
      //  throw new logic_error("No connection to DB");

    vector<string> out;
    out.clear();

    string query = "SHOW COLUMNS FROM " + tableName;

    if(mysql_query(&dbconn,query.c_str()) != 0)
    {
        cout<< "DB collectColumns ERROR: " + getError() << endl;
        return out;
    }

    MYSQL_RES *result = mysql_store_result(&dbconn);

    if(!result)
    {
        cout<< "DB collectColumns ERROR: " + getError() << endl;
        return out;
    }

    MYSQL_ROW row;

    row = mysql_fetch_row(result);
    while(row != nullptr)
    {
        out.push_back( row[0] );
        row = mysql_fetch_row(result);
    }

    mysql_free_result(result);
    return out;
}

bool
DBMySQL::ping()
{
    mtx.lock();
    bool res =  mysql_ping( &dbconn ) == 0;

    if(!res)
        this->connected = false;

    mtx.unlock();
    return res;
}

DB_ROWS
DBMySQL::select(vector<string> fields, string table)
{
    //if(!this->connected)
    //    throw new logic_error("No connection to DB");

    mtx.lock();
    DB_ROWS out;
    out.clear();

    if(fields.size() == 0)
    {
        if(columns.find(table) != columns.end())
        {
            fields = columns.at(table);
        }
        else
        {
            fields = collectColumns(table);
        }
    }

    string query = "SELECT `" + xString::join(fields,"`,`") + "` FROM " + table;

    if( mysql_query(&dbconn, query.c_str() ) != 0 )
    {
        cout<< "DB SELECT ERROR: " + getError() << endl;
        mtx.unlock();
        return out;
    }

    MYSQL_RES *dbres = mysql_store_result(&dbconn);

    // ���� ��� ��� ������ ������ 0 -> Name string colums[ mysql_num_rows() ]
    if( dbres == NULL)
    {
        cout << "^1DB SELECT ERROR: Can't store result" << endl;
        mtx.unlock();
        return out;
    }

    MYSQL_ROW dbrow;
    while( (dbrow = mysql_fetch_row(dbres)) != NULL )
    {
        DB_ROW row;
        for( unsigned int i = 0; i < mysql_num_fields(dbres); i++ )
        {
            row[ fields[ i ] ] = string( dbrow[ i ] );
        }
        out.push_back( row );
    }

    mysql_free_result(dbres);
    mtx.unlock();
    return out;
}

DB_ROWS
DBMySQL::select(vector<string> fields, string table, DB_WHERE where)
{
    //if(!this->connected)
    //    throw new logic_error("No connection to DB");

    mtx.lock();
    DB_ROWS out;
    out.clear();

    if(fields.size() == 0)
    {
        if(columns.find(table) != columns.end())
        {
            fields = columns.at(table);
        }
        else
        {
            fields = collectColumns(table);
        }
    }

    string query = "SELECT `" + xString::join(fields,"`,`") + "` FROM " + table + " WHERE 1=1";

    if(where.size() != 0)
    {
        for (DB_ROW::iterator it=where.begin(); it!=where.end(); ++it)
        {
            if(it->first.size() == 0 || it->second.size() == 0 )
                continue;

            query += " AND `" + it->first + "` LIKE '" + it->second + "'";
        }
    }

    if( mysql_query(&dbconn, query.c_str() ) != 0 )
    {
        cout<< "DB SELECT ERROR: " + getError() << endl;
        mtx.unlock();
        return out;
    }

    MYSQL_RES *dbres = mysql_store_result(&dbconn);

    // ���� ��� ��� ������ ������ 0 -> Name string colums[ mysql_num_rows() ]
    if( dbres == NULL)
    {
        cout << "^1DB SELECT ERROR: Can't store result" << endl;
        mtx.unlock();
        return out;
    }

    MYSQL_ROW dbrow;
    while( (dbrow = mysql_fetch_row(dbres)) != NULL )
    {
        DB_ROW row;
        for( unsigned int i = 0; i < mysql_num_fields(dbres); i++ )
        {
            row[ fields[ i ] ] = string( dbrow[ i ] );
        }
        out.push_back( row );
    }

    mysql_free_result(dbres);
    mtx.unlock();
    return out;
}

bool
DBMySQL::exec( string query )
{
	return exec( query.c_str() );
}

bool
DBMySQL::exec( const char *query )
{
    //if(!this->connected)
    //   throw new logic_error("No connection to DB");

    mtx.lock();

	if( mysql_query(&dbconn, query ) != 0 )
	{
	    cout<< "DB EXEC ERROR: " + getError() << endl;
	    mtx.unlock();
		return false;
	}
    mtx.unlock();
	return true;
}

bool
DBMySQL::update( string table, DB_ROW fields, DB_WHERE where )
{
    //if(!this->connected)
    //    throw new logic_error("No connection to DB");

	string query = "UPDATE " + table + " SET ";

    bool first = true;
    for( auto row: fields )
    {
        if( !first )
            query += ", ";

        first = false;

        query += row.first + "='" + row.second + "'";
    }

    if(where.size() != 0)
    {
        query += " WHERE 1=1";
        for (DB_ROW::iterator it=where.begin(); it!=where.end(); ++it)
        {
            query += " AND " + it->first + " LIKE '" + it->second + "'";
        }
    }

    if(debug)
        cout << query << endl;
    return exec( query );
}

unsigned int
DBMySQL::insert( string table, DB_ROW arFields)
{
    //if(!this->connected)
    //    throw new logic_error("No connection to DB");

    string fields;
    string values;

    bool first = true;
    for( auto row: arFields )
    {
        if( !first )
        {
            fields += ", ";
            values += ", ";
        }


        first = false;

        fields += row.first;
        values += "'" + row.second + "'";
    }

    string query = "INSERT INTO " + table + " ("+fields+") VALUES ("+values+")";


    if(debug)
        cout << query << endl;
    if( exec( query ) )
    {
        return getLastID();
    }
    else
        return -1;
}

int
DBMySQL::getLastID()
{
    //if(!this->connected)
    //    throw new logic_error("No connection to DB");

    const char* query = "SELECT LAST_INSERT_ID() as id";
    int ID = -1;

    mtx.lock();

    if( mysql_query(&dbconn, query ) != 0 )
    {
        cout<< "DB getLastID ERROR: " + getError() << endl;
        mtx.unlock();
        return ID;
    }

    MYSQL_RES *dbres = mysql_store_result(&dbconn);

    if( dbres == NULL)
    {
        cout << "^1DB getLastID ERROR: Can't store result" << endl;
        mtx.unlock();
        return ID;
    }

    MYSQL_ROW dbrow;
    if( (dbrow = mysql_fetch_row(dbres)) != NULL )
    {
        ID = atoi(dbrow[0]);
    }

    mysql_free_result(dbres);
    mtx.unlock();
    return ID;
}
