#include "tools.h"

namespace tools
{
	void log(const char *text)
	{
		FILE *file;

		file = fopen("cruise.log", "a");
		fputs(text, file);
		fputs("\n", file);
		fclose(file);
	}

	string convert_encoding(const string& data, const string& from, const string& to)
	{
		if (data.empty())
		{
			return string();
		}
		iconv_t convert_hnd = iconv_open(to.c_str(), from.c_str());
		if (convert_hnd == (iconv_t)(-1))
		{
			throw logic_error("unable to create convertion descriptor");
		}

		#if _LIBICONV_VERSION == 0x0109
			const char* in_ptr = const_cast<char*>(data.c_str());
		#else
			char* in_ptr = const_cast<char*>(data.c_str());
		#endif // _LIBICONV_VERSION

		size_t in_size = data.size();
		vector<char> outbuf(6 * data.size());
		char* out_ptr = &outbuf[0];
		size_t reverse_size = outbuf.size();

		size_t result = iconv(convert_hnd, &in_ptr, &in_size, &out_ptr, &reverse_size);
		iconv_close(convert_hnd);
		if (result == (size_t)(-1))
		{
			throw logic_error("unable to convert");
		}
		return string(outbuf.data(), outbuf.size() - reverse_size);
	}

	string toCP1251(const string& data)
	{
		return convert_encoding(data,"UTF-8","CP1251");
	}


	struct tm * GetLocalTime()
	{
		time_t rawtime;
		tm *timeStruct;

		time (&rawtime);

		timeStruct = localtime (&rawtime);
		timeStruct->tm_year += 1900;
		timeStruct->tm_mon += 1;
		timeStruct->tm_wday += 1;
		timeStruct->tm_yday += 1;

		return timeStruct;
	}
}

Vector2::Vector2()
{
	this->x = 0;
	this->y = 0;
}

Vector2::Vector2(double x, double y)
{
	this->x = x;
	this->y = y;
}

Vector2 Vector2::Rotate(double a)
{
	double x1 = x * cos(a) - y * sin(a);
	double y1 = x * sin(a) + y * cos(a);

	return Vector2(x1, y1);
}
Vector2 Vector2::Rotate(double a, Vector2 relative)
{
	double x1 = relative.x + (x - relative.x) * cos(a*M_PI/180.0) - (y - relative.y) * sin(a*M_PI/180.0);
	double y1 = relative.y + (x - relative.x) * sin(a*M_PI/180.0) + (y - relative.y) * cos(a*M_PI/180.0);
	return Vector2(x1, y1);
}

int f2i(double f)
{
	if(f >= 0)
		return (int) (f + 0.5);
	else
		return (int) (f - 0.5);
}
