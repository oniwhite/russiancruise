#include "xString.h"
using namespace std;
// split: receives a char delimiter; returns a vector of strings
// By default ignores repeated delimiters, unless argument rep == 1.
vector<xString>&
xString::split( const char delim, int rep)
{
    if (!flds.empty()) flds.clear();  // empty vector if necessary
    string work = data();
    string buf = "";
    unsigned int i = 0;
    while (i < work.length())
    {
        if (work[i] != delim)
            buf += work[i];
        else if (rep == 1)
        {
            flds.push_back(buf);
            buf = "";
        }
        else if (buf.length() > 0 && flds.empty())
        {
            flds.push_back(buf);
            buf = work.substr((size_t)i+1);
            i = work.length();
        }
        i++;
    }
    if (!buf.empty())
        flds.push_back(buf);
    return flds;
}

string
xString::join(vector<string> v, string glue)
{
    string result;
    for (size_t i = 0, i_end = v.size(); i < i_end; ++i) {
        result += (i ? glue : "");  // main line in this blog-post
        result += v[i];
    }
    return result;
}

string
xString::trim()
{
    string s = data();
    auto wsfront=find_if_not(s.begin(),s.end(),[](int c){return isspace(c);});
    auto wsback=find_if_not(s.rbegin(),s.rend(),[](int c){return isspace(c);}).base();
    return (wsback<=wsfront ? string() : string(wsfront,wsback));
}

int
xString::asInt(){return atoi(data());}

long int
xString::asLong(){return atol(data());}

double
xString::asDouble(){return atof(data());}

float
xString::asFloat(){return atof(data());}

string xString::toLower()
{
    string res = data();
    transform(res.begin(),res.end(),res.begin(),::tolower);
    return res;
}

string xString::toUpper()
{
    string res = data();
    transform(res.begin(),res.end(),res.begin(),::toupper);
    return res;
}


string ToString (int i)
{
	return StringFormat("%d",i);
}

string ToString (unsigned int i)
{
	return StringFormat("%d",i);
}

string ToString (long unsigned int i)
{
	return StringFormat("%l",i);
}

string ToString (unsigned char b)
{
	return StringFormat("%d",b);
}

string ToString (bool b)
{
	if(b)
		return "true";
	else
		return "false";
}

string ToString (float f)
{
	return StringFormat("%f",f);
}

string ToString (double d)
{
	return StringFormat("%f",d);
}


string StringFormat(const string fmt_str, ...)
{
    int final_n, n = fmt_str.size() * 2; /* reserve 2 times as much as the length of the fmt_str */
    string str;
    unique_ptr<char[]> formatted;
    va_list ap;
    while(1) {
        formatted.reset(new char[n]); /* wrap the plain char array into the unique_ptr */
        strcpy(&formatted[0], fmt_str.c_str());
        va_start(ap, fmt_str);
        final_n = vsnprintf(&formatted[0], n, fmt_str.c_str(), ap);
        va_end(ap);
        if (final_n < 0 || final_n >= n)
            n += abs(final_n - n + 1);
        else
            break;
    }
    return string(formatted.get());
}
