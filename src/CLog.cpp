#include "CLog.h"

CLog::CLog()
{
    //ctor
}

bool
CLog::log(string className, string type, string message)
{
    DB_ROW log;
    log["module"] = className;
    log["type"] = type;
    log["message"] = message;

    return (DBMySQL::Get()->insert("log",log)) > 0;
}
