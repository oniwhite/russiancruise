#include "RCMain.h"
#include <version.h>
#include "tools.h"


DBMySQL *db;


void CreateClasses()
{
    classes.push_back( RCMessage::getInstance() );
    classes.push_back( RCCore::getInstance() );
    classes.push_back( RCBank::getInstance() );

#ifdef _RC_ENERGY_H
    classes.push_back( RCEnergy::getInstance() );
#endif

#ifdef _RC_LEVEL_H
    classes.push_back( RCDL::getInstance() );
#endif

#ifdef _RC_CHEAT_H
    classes.push_back( RCAntCheat::getInstance() );
#endif

#ifdef _RC_STREET_H
    classes.push_back( RCStreet::getInstance() );
#endif

#ifdef _RC_LIGHT_H
    classes.push_back( RCLight::getInstance() );
#endif

#ifdef _RC_PIZZA_H
    classes.push_back( RCPizza::getInstance() );
#endif

#ifdef _RC_TAXI_H
    classes.push_back( RCTaxi::getInstance() );
#endif

#ifdef _RC_POLICE_H
    classes.push_back( RCPolice::getInstance() );
#endif // _RC_POLICE_H

#ifdef _RC_ROADSIGN
    classes.push_back( RCRoadSign::getInstance() );
#endif // _RC_ROADSIGN

#ifdef _RC_QUEST_H
    classes.push_back( RCQuest::getInstance() );
#endif // _RC_QUEST_H

#ifdef _RC_AUTOSCHOOL_H
    classes.push_back( RCAutoschool::getInstance() );
#endif // _RC_AUTOSCHOOL_H

#ifdef RCEVA_H
    classes.push_back( RCEva::getInstance() );
#endif // RCEVA_H

#ifdef RCDRUGS_H
    classes.push_back( RCDrugs::getInstance() );
#endif // RCDRUGS_H

#ifdef RCITEMS_H
    classes.push_back( RCItems::getInstance() );
#endif // RCITEMS_H
}

void InitClasses()
{
    for(auto cl = classes.begin(); cl != classes.end(); ++cl )
    {
        (*cl)->init(config["dataDir"].asCString());
    }
}

void Save(byte UCID)
{
    for(auto cl = classes.begin(); cl != classes.end(); ++cl )
    {
        (*cl)->Save(UCID);
    }
}

void SaveAll(bool SaveBonus)
{
    for(auto cl = classes.begin(); cl != classes.end(); ++cl )
    {
        (*cl)->SaveAll();
    }

    if(SaveBonus)
    {
        RCCore::getInstance()->SaveBonuses();
    }

    RCBaseClass::CCText("^2DATA SAVED");
}

void InsimMSO()
{
    IS_MSO* packet = (struct IS_MSO*)insim->get_packet();

    string Message = packet->Msg + packet->TextStart;

    //!save
    if (Message == "!save")
    {
        if(!RCCore::getInstance()->CanSave(packet->UCID))
            return;

        Save(packet->UCID);
        return;
    }

        //!EXIT
    if (Message == "!exit" && (RCCore::getInstance()->isAdmin(packet->UCID) || packet->UCID == 0) )
    {
        insim->SendMTC(255, "^1| ^3Russian Cruise: ^7^C���������� ������");
        SaveAll(true);

        ok=0;

        return;
    }
}


/****************************************/

int CoreConnect()
{

    if (insim->init() < 0)
    {
        RCBaseClass::CCText("^3RCMain:\t^1InSim can't connect to server");
        return -1;
    }

    RCBaseClass::CCText("^3RCMain:\t\t^2InSim connected");

    insim->SendTiny(TINY_RST,255);
    insim->SendTiny(TINY_NCN,255);
    insim->SendTiny(TINY_NPL,255);
    insim->SendTiny(TINY_NCI,255);

    return 1;
}

int CoreReconnect()
{
    insim->disconnect();

    RCBaseClass::CCText("^3RCMain:\t^1InSim disconnected");
    RCBaseClass::CCText("^3RCMain:\t^3InSim wait 1 minute and reconect");
    Sleep(60000);

    if (insim->init() < 0)
    {
        RCBaseClass::CCText("^3RCMain:\t^1InSim can't connect to server");
        return -1;
    }

    insim->SendTiny(TINY_RST,255);
    insim->SendTiny(TINY_NCN,255);
    insim->SendTiny(TINY_NPL,255);
    insim->SendTiny(TINY_NCI,255);

    return 1;
}


void ReadConfig(string file)
{


    ifstream readf (file, ios::in);

    if(!readf.is_open())
    {
        cout <<"RCMain ERROR: file " << file << " not found" << endl;
        return ;
    }

    bool readed = configReader.parse( readf, config, false );

    readf.close();

    if ( !readed )
    {
        // report to the user the failure and their locations in the document.
        cout  << "Failed to parse configuration\n"
              << configReader.getFormattedErrorMessages();
        return;
    }

    return;
}

/*********************************************/

void ThreadMci ()
{
    while (true)
    {
        if (insim->udp_next_packet() < 0)
            continue;

        try
        {
            for(auto cl = classes.begin(); cl != classes.end(); ++cl )
            {
                (*cl)->upd_next_packet();
            }
        }
        catch(const logic_error& lerror)
        {
            RCBaseClass::CCText(string("^1MCI ERROR: ") + lerror.what() );
        }
    }
};

void ThreadSave ()
{
    // First time we load Items from database
    Items::Get();

    time_t seconds;

    int myReconnect = 0;

    while (ok > 0)
    {
        seconds = time (NULL);
        if (seconds % 1800 == 0) //every 30 minute
        {
            try
            {
                SaveAll(true);
            }
            catch(const logic_error& lerror)
            {
                RCBaseClass::CCText(string("^1SAVE ERROR: ") + lerror.what() );
            }


            int myPing = db->ping();
            if( myPing != 0 )
            {
                RCBaseClass::CCText( db->getError() );

                if (myReconnect > 2)
                {
                    if ( db->connect() == false )
                    {
                        RCBaseClass::CCText("^3RCMain: ^1Can't connect to MySQL server");
                        RCBaseClass::CCText( db->getError() );

                    }
                    else
                    {
                        RCBaseClass::CCText("^3RCMain:\t\t^2Connected to MySQL server");
                        myReconnect = 0;
                    }
                }
            }
            else
                myReconnect = 0;
        }

        Sleep(1000);
    }
};

void ThreadEvent ()
{
    Sleep(2000);

    RCCore::getInstance()->ReadBonuses();

    while (ok > 0)
    {
        try
        {
            for(auto cl = classes.begin(); cl != classes.end(); ++cl )
                (*cl)->Event();

        }
        catch(const logic_error& lerror)
        {
            RCBaseClass::CCText(string("^1EVENT ERROR: ") + lerror.what() );
        }

        Sleep(500);
    }
};

void SignalHandler(int sig)
{
    RCBaseClass::CCText("^1| ^3Russian Cruise: ^7Exit with code: " + ToString(sig));
    insim->SendMTC(255, "^1| ^3Russian Cruise: ^7^C����������, ���������� ������");
    RCBaseClass::CCText("^1| ^3Russian Cruise: ^7Start save");
    SaveAll(true);
    Items::Get()->SaveItems();
    ok = 0;
    RCBaseClass::CCText("^1| ^3Russian Cruise: ^7Finiish save");
    exit(sig);
}

int main(int argc, char* argv[])
{
    #ifdef CIS_LINUX
    signal(SIGINT,SignalHandler); // ctrl+c
    signal(SIGKILL,SignalHandler); // kill
    signal(SIGQUIT,SignalHandler); // ctrl + '\'
    signal(SIGTERM,SignalHandler); // kill
    signal(SIGTSTP,SignalHandler); // ctrl + z
    #endif // CIS_LINUX

    setlocale(LC_CTYPE, "ru_RU.utf8"); // ����� ������� ��������� ������

    if( argc < 2 )
    {
        cout << "ERROR!!! Need params" << endl;
        cout << "Use like ./RCMod /path/to/config.json" << endl;
        return -1;
    }



    ReadConfig(argv[1]);

    cout << config["dataDir"].asString() << endl;

    COptions::setRoot(config["dataDir"].asString());


    db = DBMySQL::Get(config["mysql"]["host"].asString(), config["mysql"]["port"].asInt(), config["mysql"]["username"].asString(), config["mysql"]["password"].asString(), config["mysql"]["database"].asString());

    while ( db->connect() == false )
    {
        RCBaseClass::CCText("^3RCMain: ^1Can't connect to MySQL server");
        RCBaseClass::CCText(db->getError());

        Sleep(5000);
    }
    RCBaseClass::CCText("^3RCMain:\t\t^2MySQL connected");

    RCCore::IS_PRODUCT_NAME = StringFormat("RC-%.12s", AutoVersion::RC_UBUNTU_VERSION_STYLE);

    insim = CInsim::getInstance(
        config["insim"]["host"].asString(),
        config["insim"]["tcpPort"].asInt(),
        RCCore::IS_PRODUCT_NAME,
        config["insim"]["password"].asString(),
        '!',
        RCCore::isf_flag,
        500,
        config["insim"]["udpPort"].asInt(),
        config["insim"]["version"].asInt()
    );

    if ( CoreConnect() < 0 )
        for (;;)
            if ( CoreReconnect() > 0 )
                break;

    if (insim->getHostVersion() < 8)
    {
        RCBaseClass::CCText("^3RCMain:\t^1INSIM VER < 8");
        return -1;
    }

    CreateClasses();
    InitClasses();

    std::thread(ThreadEvent).detach();
    std::thread(ThreadSave).detach();

    if(config["insim"]["udpPort"].asInt() > 0)
    {
        std::thread(ThreadMci).detach();
    }

    RCBaseClass::CCText("^6Configuration loading:");

    while (ok > 0)
    {
        if (insim->next_packet() < 0)
            for (;;)
                if ( CoreReconnect() > 0 )
                    break;

        try
        {
            switch (insim->peek_packet())
            {
            case ISP_MSO:
                InsimMSO();
                break;

            }

           for(auto cl = classes.begin(); cl != classes.end(); ++cl )
           {
                (*cl)->next_packet();
           }
        }
        catch(const logic_error& lerror)
        {
            RCBaseClass::CCText(string("^1 ERROR: ") + lerror.what() );
        }
    }

    if (insim->disconnect() < 0)
        return 0;

    CInsim::removeInstance();

    return 0;
}
