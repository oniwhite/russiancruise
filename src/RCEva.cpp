#include "RCEva.h"

RCEva*
RCEva::self = nullptr;

RCEva*
RCEva::getInstance()
{
    if(!self)
        self = new RCEva();

    return self;
}

RCEva::RCEva()
{
    ClassName = "RCEva";
}

RCEva::~RCEva()
{

}

int
RCEva::init(const char *Dir)
{
    strcpy(RootDir,Dir);

    this->db = DBMySQL::Get();

    this->insim = CInsim::getInstance();
    this->msg = RCMessage::getInstance();
    this->dl = RCDL::getInstance();
    this->bank = RCBank::getInstance();
    this->taxi = RCTaxi::getInstance();
    this->police = RCPolice::getInstance();
    this->street = RCStreet::getInstance();

    CCText("^3"+ClassName+":\t\t^2inited");
    return 1;
}

void RCEva::InsimNCN( struct IS_NCN* packet )
{
    if ( packet->UCID == 0 )
        return;

    players[packet->UCID].UName = packet->UName;
    players[packet->UCID].PName = packet->PName;
    players[packet->UCID].Admin = packet->Admin;

    if(!packet->Admin)
        players[packet->UCID].Admin = this->isAdmin(packet->UName);

    ReadUser(packet->UCID);

    NumP = packet->Total;
}

void RCEva::InsimNPL( struct IS_NPL* packet )
{
    PLIDtoUCID[packet->PLID] = packet->UCID;
    players[packet->UCID].PName = packet->PName;
    players[packet->UCID].CName = expandCar(packet->CName);
    players[packet->UCID].PLID = packet->PLID;

    if (players[packet->UCID].WorkDeal && players[packet->UCID].CName != "UFR" && players[packet->UCID].CName != "XFR")
    {
        insim->SendMTC(packet->UCID, msg->_(packet->UCID, "EvcNeedCar"));
        insim->SendMTC(packet->UCID, msg->_(packet->UCID, "EvcUndeal"));
        players[packet->UCID].WorkDeal = false;
        players[packet->UCID].WorkAccept = false;
        return;
    }

    if(players[packet->UCID].WorkDeal && !players[packet->UCID].WorkAccept)
    {
        players[packet->UCID].WorkAccept = true;
        //CCText(players[packet->UCID].UName + " evc begin track");
    }
}

void RCEva::InsimMSO( struct IS_MSO* packet )
{
    byte UCID = packet->UCID;
    if (UCID == 0)
        return;

    if (packet->UserType != MSO_PREFIX)
        return;

    xString Message = packet->Msg + packet->TextStart;
    vector<xString> args = Message.split(' ', 1);

    // ���� ���
    if (CheckPosition(4, zone.dealX, zone.dealY, players[UCID].Info.X / 65536, players[UCID].Info.Y / 65536))
    {
        if (Message == "!deal")
        {
            if (players[UCID].WorkAccept)
            {
                insim->SendMTC(UCID, msg->_(UCID, "EvcAlrdDeal"));
                return;
            }

            if (GetEvaCount() > (int)NumP/10)
            {
                insim->SendMTC(UCID, msg->_(UCID, "EvcNoJob"));
                return;
            }

            if (players[UCID].CName != "UFR" && players[UCID].CName != "XFR")
            {
                insim->SendMTC(UCID, msg->_(UCID, "EvcNeedCar"));
                return;
            }

            if (players[UCID].PName.find("^3[^1EVC^3]") == string::npos && players[UCID].PName.find("^3[^C^1���^3]") == string::npos )
            {
                insim->SendMTC(UCID, msg->_(UCID, "EvcNeedPref"));
                return;
            }

            insim->SendMTC(UCID, msg->_(UCID, "EvcDeal"));
            players[UCID].WorkDeal = true;
            players[UCID].WorkAccept = true;
        }

        if (Message == "!undeal")
        {
            if (players[UCID].WorkAccept)
            {
                insim->SendMTC(UCID, msg->_(UCID, "EvcUndeal"));
                players[UCID].WorkDeal = false;
                players[UCID].WorkAccept = false;
            }
            else
                insim->SendMTC(UCID, msg->_(UCID, "EvcAlrdUndeal"));
        }
    }

    if (Message == "!estat")
    {
        insim->SendMTC(UCID, StringFormat(msg->_(UCID, "EvcCapital"), Capital));

        if (GetEvaCount() == 0)
        {
            insim->SendMTC(UCID, msg->_(UCID, "EvcNoWorkers"));
        }
        else
        {
            insim->SendMTC(UCID, StringFormat(msg->_(UCID, "EvcWorkCount"), GetEvaCount(), (int)NumP/10 + 1));

            for (auto p: players)
			{
			    if (players[p.first].WorkDeal)
                {
                    if (players[p.first].WorkAccept)
                        insim->SendMTC(packet->UCID, StringFormat("^3| ^9%s", players[p.first].PName.c_str()));
                    else
                        insim->SendMTC(packet->UCID, StringFormat("^3| ^9%s - ^C�� ����� ������� �����", players[p.first].PName.c_str()));
                }
			}
        }
    }

    if (Message == "!up") {

        if (PLIDtoUCID.find(packet->PLID) == PLIDtoUCID.end())
            return;

        Json::Value cars = RCCore::getInstance()->getCars();

        if (cars[players.at(UCID).CName]["category"].asString() == "A") {
            insim->ResetCar(packet->PLID, players.at(UCID).Info.X, players.at(UCID).Info.Y, players.at(UCID).Info.Z, players.at(UCID).Info.Heading, false);
        }

    }

    if (Message == "!pit" || Message == "!^C���")
    {
        if(PLIDtoUCID.find(packet->PLID) == PLIDtoUCID.end())
            return;

        if (street->CurentStreetNum(UCID)  == 0)
        {
            insim->SendMTC(UCID, msg->_(UCID, "EvcCntInPit"));
            return;
        }

        if (bank->GetCash(UCID) < PIT_PAY)
        {
            insim->SendMTC(UCID, msg->_(UCID, "NoManyPay"));
            return;
        }

        #ifdef _RC_POLICE_H
        if(police->IsCop(UCID))
        {
            Pitlane(UCID);
            return;
        }

        if (police->InPursuite(UCID) == 1)
        {
            char Msg[64];
            sprintf(Msg, "/pitlane %s", players[UCID].UName.c_str());
            insim->SendMST(Msg);

            int pay = 10000;

            insim->SendMTC(UCID, StringFormat(msg->_(UCID, "2700"), pay));
            bank->RemCash(UCID, pay);
            bank->AddToBank(pay);

            if (dl->GetSkill(UCID) > 10 )
                dl->RemSkill(UCID, 10);

            return;
        }
        #endif

        if(players[UCID].WorkAccept)
        {
            Pitlane(UCID);
            return;
        }

        // ���� �� ����� ������������ �����
        if (GetEvaOnTrackCount() > 0)
        {
            int L = 70, // ������ �����
            T = 80, // ������ ������
            W = 64; // ������

            insim->SendButton(255, UCID, 91, L, T, W, 40, 32, "");                                      // ���
            insim->SendButton(255, UCID, 92, L + 1, T, W - 2, 6, 1, msg->_(UCID, "EvcWndTitle"));       // ���������
            insim->SendButton(199, UCID, 93, L + 1, T + 06, W - 2, 6, 32 + 64 + 8, StringFormat(msg->_(UCID, "EvcTake"), EVC_TAKE));
            insim->SendButton(199, UCID, 94, L + 1, T + 12, W - 2, 6, 32 + 64 + 8, StringFormat(msg->_(UCID, "EvcRepair"), EVC_REPAIR));
            insim->SendButton(199, UCID, 95, L + 1, T + 18, W - 2, 6, 32 + 64 + 8, StringFormat(msg->_(UCID, "EvcRespawn"), EVC_RESPAWN));
            insim->SendButton(255, UCID, 96, L + 1, T + 25, W - 2, 1, 16, "");                          // �������
            insim->SendButton(199, UCID, 97, L + 1, T + 27, W - 2, 6, 32 + 64 + 8 + 5, StringFormat(msg->_(UCID, "EvcFastPit"), EVC_FASTPIT));
            insim->SendButton(199, UCID, 98, L + 20, T + 33, W - 40, 6, 32 + 8 + 7, msg->_(UCID, "EvcWndCancel"));
        }
        else
            Pitlane(UCID);
    }
}

void RCEva::InsimPLP( struct IS_PLP *packet )
{
    if(PLIDtoUCID.find(packet->PLID) == PLIDtoUCID.end())
        return;

    byte UCID = PLIDtoUCID[packet->PLID];

    /*byte UCID = PLIDtoUCID[packet->PLID];
    players[UCID].NeedToPit = false;
    players[UCID].pitTime = false;
    insim->SendBFN(255, 91, 91+32);
    ClearButtonClock(UCID);
    if(players[UCID].Worker)
    {
        SaveEvaStat(UCID);
        players[UCID].Worker = false;
    }*/

    if(players[UCID].WorkDeal && players[UCID].WorkAccept)
    {
        players[UCID].WorkAccept = false;
    }

    if (players[UCID].NeedToPit > 0)
    {
        EvcFalse(UCID);
    }

    PLIDtoUCID.erase( packet->PLID );
}

void RCEva::InsimPLL( struct IS_PLL *packet )
{
    if(PLIDtoUCID.find(packet->PLID) == PLIDtoUCID.end())
        return;

    byte UCID = PLIDtoUCID[packet->PLID];

    /*byte UCID = PLIDtoUCID[packet->PLID];
    players[UCID].NeedToPit = false;
    players[UCID].pitTime = false;
    insim->SendBFN(255, 91, 91+32);
    ClearButtonClock(UCID);
    if(players[UCID].Worker)
    {
        SaveEvaStat(UCID);
        players[UCID].Worker = false;
    }*/

    if(players[UCID].WorkDeal && players[UCID].WorkAccept)
    {
        players[UCID].WorkAccept = false;
    }

    if (players[UCID].NeedToPit > 0)
    {
        EvcFalse(UCID);
    }

    PLIDtoUCID.erase( packet->PLID );
}

void RCEva::InsimCNL( struct IS_CNL *packet )
{
    byte UCID = packet->UCID;

    if(players[UCID].WorkDeal && players[UCID].WorkAccept)
    {
        //CCText(players[packet->UCID].UName + " evc leave server");
    }

    if (players[UCID].NeedToPit > 0)
    {
        EvcFalse(UCID);
    }

    players.erase(UCID);
    NumP = packet->Total;

    DB_ROW upd;
    upd["Capital"] = ToString(Capital);
    db->update("service_station", upd, {});
}

void RCEva::InsimCPR( struct IS_CPR *packet )
{
    byte UCID = packet->UCID;

    players[UCID].PName = packet->PName;

    if (players[UCID].WorkDeal && players[UCID].PName.find("^3[^1EVC^3]") == string::npos && players[UCID].PName.find("^3[^C^1���^3]") == string::npos )
    {
        insim->SendMTC(UCID, msg->_(UCID, "EvcUndeal"));
        players[UCID].WorkDeal = false;
        players[UCID].WorkAccept = false;
        insim->SendBFNAll(UCID);
    }
}

void
RCEva::InsimBTC( struct IS_BTC* packet )
{
    byte UCID = packet->UCID;

    // ����� ����� �����������
    if(packet->ClickID == 230)
    {
        if(players[packet->ReqI].NeedToPit > 0)
        {
            insim->SendMTC(UCID, msg->_(UCID, "EvcAlrdCall"));
            return;
        }

        if (street->CurentStreetNum(packet->ReqI)  == 0)
        {
            insim->SendMTC(UCID, msg->_(UCID, "EvcCntInPit"));
            return;
        }

        if (time(NULL) - players[packet->ReqI].TimeCallToPit < 300)
        {
            insim->SendMTC(UCID, msg->_(UCID, "EvcCntTime"));
            return;
        }

        players[packet->ReqI].NeedToPit = EVC_POLICE;
        players[packet->ReqI].TimeCallToPit = time(NULL);

        if(players[packet->ReqI].WorkAccept)
        {
            Pitlane(packet->ReqI);
            return;
        }
        else
        {
            insim->SendMTC(UCID, StringFormat(msg->_(UCID, "EvcPoliceCall"), players[packet->ReqI].PName.c_str()));
            insim->SendMTC(packet->ReqI, StringFormat(msg->_(packet->ReqI, "EvcPoliceCall2"), players[UCID].PName.c_str()));
            for(auto& play: players)
                if (players[play.first].WorkDeal)
                    insim->SendMTC(play.first,
                        StringFormat(msg->_(play.first, "EvcCall2"),
                                    players[packet->ReqI].PName.c_str(),
                                    "������������ � �����" ));
        }
    }

    if(packet->ReqI == 199 && !players[UCID].WorkAccept && !police->IsCop(UCID))
    {
        if (packet->ClickID == 98)
        {
            insim->SendBFN(UCID, 91, 98);
            return;
        }

        if(PLIDtoUCID.find(players[UCID].PLID) == PLIDtoUCID.end())
            return;

        if (players[UCID].Info.Speed * 360 / 32768 > 0)
        {
            insim->SendMTC(UCID, msg->_(UCID, "EvcCntSpeed"));
            return;
        }

        if (packet->ClickID == 97)
        {
            Pitlane(UCID, EVC_FASTPIT);
            return;
        }

        if (players[UCID].NeedToPit > 0)
        {
            insim->SendMTC(UCID, msg->_(UCID, "EvcCntCall"));
            return;
        }

        if (time(NULL) - players[UCID].TimeCallToPit < 300)
        {
            insim->SendMTC(UCID, msg->_(UCID, "EvcCntTime"));
            return;
        }

        if (packet->ClickID == 93)
            players[UCID].NeedToPit = EVC_TAKE;

        if (packet->ClickID == 94)
            players[UCID].NeedToPit = EVC_REPAIR;

        if (packet->ClickID == 95)
            players[UCID].NeedToPit = EVC_RESPAWN;

        players[UCID].TimeCallToPit = time(NULL);
        insim->SendMTC(UCID, msg->_(UCID, "EvcCall"));

        players[UCID].EvaX = players[UCID].Info.X / 65536;
        players[UCID].EvaY = players[UCID].Info.Y / 65536;

        insim->SendBFN(UCID, 91, 98);

        for(auto& play: players)
            if (players[play.first].WorkDeal)
                insim->SendMTC(play.first,
                    StringFormat(msg->_(play.first, "EvcCall2"),
                                players[UCID].PName.c_str(),
                                (players[UCID].NeedToPit == EVC_TAKE ? "�����������" :
                                (players[UCID].NeedToPit == EVC_REPAIR ? "���������������" :
                                (players[UCID].NeedToPit == EVC_RESPAWN ? "������������ � �����" :
                                (players[UCID].NeedToPit == EVC_POLICE ? "������������ � �����" : "n/a"))))
                                ));
    }
}

void RCEva::InsimMCI(struct IS_MCI *packet)
{
    for (int i = 0; i < packet->NumC; i++)
    {
        byte UCID = PLIDtoUCID[ packet->Info[i].PLID ];
        players[UCID].Info = packet->Info[i];

        int X = packet->Info[i].X / 65536;
        int Y = packet->Info[i].Y / 65536;

        // ���� ���
        if (CheckPosition(4, zone.dealX, zone.dealY, X, Y))
        {
            if (players[UCID].Zone != 1)
            {
                players[UCID].Zone = 1;
                insim->SendMTC(UCID, msg->_(UCID, "sto_head"));

                if (!players[UCID].WorkAccept)
                    insim->SendMTC(UCID, msg->_(UCID, "stodeal")); // deal
                else
                    insim->SendMTC(UCID, msg->_(UCID, "stoundeal")); // undeal
            }
        }
        else
            players[UCID].Zone = 0;
    }
}

void RCEva::EvcFalse(byte UCID)
{
    insim->SendMTC(UCID, msg->_(UCID, "EvcFine"));
    bank->RemCash(UCID, players[UCID].NeedToPit * 2);
    Capital += players[UCID].NeedToPit;

    insim->SendBFN(UCID, 212);

    insim->SendBFN(UCID, 91, 98);

    for(auto& play: players)
        if (players[play.first].WorkDeal)
        {
            insim->SendBFN(play.first, 91, 101);
            insim->SendMTC(play.first, StringFormat(msg->_(play.first, "EvcFine2"), players[UCID].PName.c_str()));
            bank->AddCash(play.first, players[UCID].NeedToPit / GetEvaCount(), true);
        }

    players[UCID].NeedToPit = 0;
}

void RCEva::Event()
{
    for (auto& play: players)
    {
        byte UCID = play.first;

        // ����� ����������� ����
        if (players[UCID].NeedToPit > 0)
        {
            time_t t = players[UCID].TimeCallToPit + 60 * 3 - time(NULL);  // ������� �������� �� ����� ������
            int S1 = (int)players[UCID].Info.Speed * 360 / 32768;

            int X2 = players[UCID].Info.X / 65536;
            int Y2 = players[UCID].Info.Y / 65536;

            int Dist = Distance(players[UCID].EvaX, players[UCID].EvaY, X2, Y2);

            if (Dist > 30 && S1 > 0)
            {
                EvcFalse(UCID);
            }

            insim->SendButton(255, UCID, 212, 130, 1, 10, 8, 32, StringFormat("^3%02d:%02d", t / 60, t%60));

            if (GetEvaOnTrackCount() == 0)
            {
                insim->SendMTC(UCID, msg->_(UCID, "EvcLeave"));
                Pitlane(UCID);
                insim->SendBFN(UCID, 212);
                CLog::log(this->ClassName,"evc",StringFormat("autoevacuated %s after leave all evc", players[UCID].UName.c_str()));
                continue;
            }

            if (players[UCID].Info.Z / 65536 < -5)
            {
                insim->SendMTC(UCID, msg->_(UCID, "EvcAuto"));
                Pitlane(UCID);
                CLog::log(this->ClassName,"evc",StringFormat("autoevacuated %s under map", players[UCID].UName.c_str()));

                for(auto& play: players)
                    if (players[play.first].WorkDeal)
                    {
                        insim->SendBFN(play.first, 91, 101);
                        insim->SendMTC(play.first, StringFormat(msg->_(play.first, "EvcAuto2"), players[UCID].PName.c_str()));
                    }
                continue;
            }

            if (t < 0)
            {
                insim->SendMTC(UCID, msg->_(UCID, "EvcDown"));
                Pitlane(UCID, 0);
                insim->SendBFN(UCID, 212);

                for(auto& pl: players)
                {
                    byte UCID2 = pl.first;

                    if (players[UCID2].WorkDeal)
                    {
                        CLog::log(this->ClassName,"evc",StringFormat("%s missed request evc %s", players[UCID2].UName.c_str(), players[UCID].UName.c_str()));
                        insim->SendMTC(UCID2, StringFormat(msg->_(UCID2, "EvcDown2"), players[UCID].PName.c_str()));
                        bank->RemCash(UCID2, 1000/GetEvaCount());
                        Capital += 1000/GetEvaCount();
                        //remSkill
                        players[UCID2].EvcLose++;

                        for(auto& play: players)
                            if (players[play.first].WorkAccept)
                                insim->SendBFN(play.first, 91, 101);
                    }
                }
            }
        }

        // ����
        if (players[UCID].WorkAccept)
        {
            int T = 100;    // ������ �� �������� ���� ������ �� ������
            int L = 160;    // �� ������ ���� ������
            byte id = 91;   // ��������� �� ������ ������
            int i = 0;

            int X1 = players[UCID].Info.X / 65536;
            int Y1 = players[UCID].Info.Y / 65536;
            int S1 = (int)players[UCID].Info.Speed * 360 / 32768;

            for(auto& pl: players)
            {
                byte UCID2 = pl.first;

                if (players[UCID2].NeedToPit > 0)
                {
                    int X2 = players[UCID2].Info.X / 65536;
                    int Y2 = players[UCID2].Info.Y / 65536;
                    int S2 = (int)players[UCID2].Info.Speed * 360 / 32768;

                    int Dist = Distance(X1, Y1, X2, Y2);

                    time_t t = players[UCID2].TimeCallToPit + 60 * 3 - time(NULL);  // ������� �������� �� ����� ������

                    string str1 = StringFormat(msg->_(UCID, "EvcGetCall"),
                                              players[UCID2].PName.c_str(),
                                              street->GetStreetName(UCID, street->CurentStreetNum(UCID2)),
                                              Dist <= 10 ? "^2" : "",
                                              Dist,
                                              t / 60,
                                              t%60);

                    string str2 = StringFormat(msg->_(UCID, "EvcGetEv"),
                                              players[UCID2].PName.c_str(),
                                              t / 60,
                                              t%60);

                    if (Dist <= 10 && S1 == 0 && S2 == 0)
                    {

                        string ProgressBar = "^2";
                        for(int j = 0; j<10; j++)
                        {
                            ProgressBar += "||||||";
                            if (j == players[UCID2].Progress)
                                ProgressBar += "^9";
                        }

                        insim->SendButton(199, UCID, id + i, L, T + 4*i, 39, 4, 32, str2);
                        i++;

                        insim->SendButton(198, UCID, id + i, L, T + 4*i, 39, 4, 32 + 7, ProgressBar);
                        insim->SendButton(198, UCID2, id + i, L, T + 4*i, 39, 4, 32 + 7, msg->_(UCID2, "EvcGetEv2") + ProgressBar);

                        players[UCID2].Progress++;
                        if (players[UCID2].Progress == 10)
                        {
                            players[UCID2].Progress = 0;

                            if (players[UCID2].NeedToPit == EVC_TAKE)
                            {
                                insim->ResetCar(players[UCID2].PLID, players[UCID2].Info.X, players[UCID2].Info.Y, players[UCID2].Info.Z, players[UCID2].Info.Heading, false);
                                insim->SendMTC(UCID, StringFormat(msg->_(UCID, "EvtGetTake"), players[UCID2].PName.c_str()));
                                insim->SendMTC(UCID2, StringFormat(msg->_(UCID2, "EvtGetTake2"), players[UCID].PName.c_str()));
                            }
                            else if (players[UCID2].NeedToPit == EVC_REPAIR)
                            {
                                insim->ResetCar(players[UCID2].PLID, players[UCID2].Info.X, players[UCID2].Info.Y, players[UCID2].Info.Z, players[UCID2].Info.Heading, true);
                                insim->SendMTC(UCID, StringFormat(msg->_(UCID, "EvcGetRepair"), players[UCID2].PName.c_str()));
                                insim->SendMTC(UCID2, StringFormat(msg->_(UCID2, "EvcGetRepair2"), players[UCID].PName.c_str()));
                            }
                            else if (players[UCID2].NeedToPit == EVC_RESPAWN)
                            {
                                insim->SendMST("/pitlane " + players[UCID2].UName);
                                insim->SendMTC(UCID, StringFormat(msg->_(UCID, "EvcGetRespawn"), players[UCID2].PName.c_str()));
                                insim->SendMTC(UCID2, StringFormat(msg->_(UCID2, "EvcGetRespawn2"), players[UCID].PName.c_str()));
                            }
                            else if (players[UCID2].NeedToPit == EVC_POLICE)
                            {
                                insim->SendMST("/pitlane " + players[UCID2].UName);
                                insim->SendMTC(UCID, StringFormat(msg->_(UCID, "EvcGetPolice"), players[UCID2].PName.c_str()));
                                insim->SendMTC(UCID2, StringFormat(msg->_(UCID2, "EvcGetPolice2"), players[UCID].PName.c_str()));
                            }

                            insim->SendBFN(UCID2, 212);
                            insim->SendBFN(UCID2, 91, 101);

                            for(auto& play: players)
                                if (players[play.first].WorkAccept)
                                    insim->SendBFN(play.first, 91, 101);

                            taxi->PassLoss(UCID2);

                            time_t t = players[UCID2].TimeCallToPit + 60 * 3 - time(NULL);              // ������� �������� �� ����� ������

                            int Pay = players[UCID2].NeedToPit;                                         // ������� �������� �����
                            int Salary = (int)((double)t / 180 * 500 + players[UCID2].NeedToPit - 500); // ������� ������� ����

                            players[UCID2].NeedToPit = 0;

                            bank->RemCash(UCID2, Pay);
                            Capital += Pay - Salary;
                            bank->AddCash(UCID, Salary, true);
                            dl->AddSkill(UCID);

                            players[UCID].EvcCount++;

                            CLog::log(this->ClassName,"evc",StringFormat("%s evacuated %s (%d/%d RUR), %d sec", players[UCID].UName.c_str(), players[UCID2].UName.c_str(), Pay, Salary, time(NULL) - players[UCID2].TimeCallToPit));
                        }
                        break;
                    }
                    else
                    {
                        /*if (players[UCID2].Progress > 0) // �� �������� ��� 2� � ����� ������
                        {
                            players[UCID2].Progress = 0;
                            insim->SendBFN(UCID2, 91, 101);
                        }*/

                        insim->SendButton(255, UCID, id + i, L, T + 4*i, 39, 4, 32, str1);
                    }

                    i++;

                    if (i < 10)
                        insim->SendBFN(UCID, id + i, id + 10);
                }
            }
        }

        // ���� ��������������
        if (players[UCID].WorkDeal)
        {
            int X = players[UCID].Info.X / 4096;

            if (X == players[UCID].LastPointX)
                players[UCID].TimeAfk++;
            else if (players[UCID].TimeAfk>0)
                players[UCID].TimeAfk = 0;

            players[UCID].LastPointX = X;

            if (players[UCID].TimeAfk > 300 * 2)    // ���� ���� ��� 5 ����� (300 ���) - ����
            {
                insim->SendMTC(UCID, msg->_(UCID, "EvcAfkFired"));
                players[UCID].TimeAfk = 0;
                players[UCID].WorkDeal = false;
                players[UCID].WorkAccept = false;
                players[UCID].EvcFired++;
            }
        }
    }
}

void RCEva::ReadConfig(const char *Track)
{
    char file[MAX_PATH];
    sprintf(file, "%s/RCEva/%s", RootDir, Track);

	msg->ReadLangDir(file);

    sprintf(file, "%s/RCEva/%s/points.txt", RootDir, Track);

    ifstream readf;
    readf.open(file, ios::in);

    if(!readf.is_open())
    {
        CCText("  ^7RCEva      ^1ERROR: ^8file " + (string)file + " not found");
        return;
    }

    while (readf.good())
    {
        char str[128];
        readf.getline(str, 128);

        if (strlen(str) > 1)
        {
            if (strstr(str, "/dealer") != NULL)
            {
                for (int i=0; i<4; i++)
                {
                    readf.getline(str, 128);
                    char * X;
                    char * Y;
                    X = strtok (str, "; ");
                    Y = strtok (NULL, "; ");
                    zone.dealX[i] = atoi(X);
                    zone.dealY[i] = atoi(Y);
                }
            }
        }
    }
    readf.close();

    DB_ROWS res = db->select({},"service_station");
    if(res.size() > 0)
    {
        DB_ROW row = res.front();

        Capital = row["Capital"].asDouble();
    }
    else
    {
        db->insert("service_station",{
                   {"Capital","1000"}}
                   );
    }

    CCText("  ^7RCEva\t\t^2OK");
}

void RCEva::Pitlane(byte UCID, int PAY)
{
    insim->SendMST("/pitlane " + players[UCID].UName);

    players[UCID].NeedToPit = 0;

    insim->SendBFN(UCID, 91, 98);

    for(auto& play: players)
        if (players[play.first].WorkAccept)
            insim->SendBFN(play.first, 91, 101);

    bank->RemCash(UCID, PAY);
    Capital += PAY;

    taxi->PassLoss(UCID);
}

void RCEva::ReadUser(byte UCID)
{
	DB_ROWS res = db->select({}, "service_stat", {{"username", players[UCID].UName}});

	if( res.size() > 0 )
	{
		DB_ROW row = res.front();

		players[UCID].EvcCount = row["count"].asInt();
		players[UCID].EvcLose= row["lose"].asInt();
		players[UCID].EvcFired = row["fired"].asInt();
	}
	else
		Save(UCID);
}

void RCEva::Save( byte UCID )
{
	string query = StringFormat("REPLACE INTO service_stat (username, count, lose, fired) VALUES ('%s', %d, %d, %d)",
								players[UCID].UName.c_str(),
								players[UCID].EvcCount,
								players[UCID].EvcLose,
								players[UCID].EvcFired);
	db->exec( query );
}

int RCEva::GetEvaCount()
{
    int i = 0;
    for(auto& play: players)
        if (players[play.first].WorkDeal)
            i++;
    return i;
}

int RCEva::GetEvaOnTrackCount()
{
    int i = 0;
    for(auto& play: players)
        if (players[play.first].WorkAccept)
            i++;
    return i;
}
