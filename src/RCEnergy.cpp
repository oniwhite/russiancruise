using namespace std;
#include "RCEnergy.h"

RCEnergy*
RCEnergy::self = nullptr;

RCEnergy*
RCEnergy::getInstance()
{
    if(!self)
        self = new RCEnergy();

    return self;
}

RCEnergy::RCEnergy()
{
    ClassName = "RCEnergy";
}

RCEnergy::~RCEnergy()
{

}

int RCEnergy::init(const char* Dir)
{
    strcpy(RootDir,Dir);

    this->db = DBMySQL::Get();
    if (!this->db)
    {
        printf("^3RCEnergy:\t^1Can't sctruct MySQL Connector\n");
        return -1;
    }

    insim = CInsim::getInstance();
    if (!insim)
    {
        printf ("^3RCEnergy:\t^1Can't struct CInsim class");
        return -1;
    }

    msg = RCMessage::getInstance();
    if (!msg)
    {
        printf ("^3RCEnergy:\t^1Can't struct RCMessage class");
        return -1;
    }

    bank = RCBank::getInstance();
    if (!msg)
    {
        printf ("^3RCEnergy:\t^1Can't struct RCBank class");
        return -1;
    }

    CCText("^3"+ClassName+":\t^2inited");
    return 0;
}


void RCEnergy::ReadConfig(const char *Track)
{
    string file;
    file = StringFormat("%s/RCEnergy/maps/%s.json", RootDir, Track );


    ifstream readf;
    readf.open(file, ios::in);

    if(!readf.is_open()) {
        CCText("  ^7RCEnergy ^1ERROR: ^8file " + file + " not found");
        return ;
    }

    bool readed = configReader.parse( readf, config, false );

    if ( !readed )
	{
		readf.close();
		// report to the user the failure and their locations in the document.
		cout  << "Failed to parse configuration\n"
				   << configReader.getFormattedErrorMessages();
		return;
	}
	readf.close();

    CCText("  ^7RCEnergy\t^2OK");
}

void RCEnergy::InsimNCN( struct IS_NCN* packet )
{
    if(packet->UCID == 0)
        return;

    players[packet->UCID].UName = packet->UName;
    players[packet->UCID].PName = packet->PName;
    players[packet->UCID].Admin = packet->Admin;

    if(!packet->Admin)
    {
        players[packet->UCID].Admin = this->isAdmin(packet->UName);
    }

    players[packet->UCID].Zone = 1;
    players[packet->UCID].InSpec = true;

    DB_ROWS result = db->select({"energy","lock_to"},"energy",{{"username",packet->UName}});

    if( result.size() != 0 )
    {
        DB_ROW row;
    	row = result.front();
        players[packet->UCID].Energy = row["energy"].asFloat();
        LockTo(packet->UCID,row["lock_to"].asInt());
    }
    else
    {
        char query[128];
        CCText("^3" + ClassName + ": ^7new user " + packet->UName);

        sprintf(query, "INSERT INTO energy (username) VALUES ('%s');", packet->UName);

        db->exec(query);

        players[ packet->UCID ].Energy = 100;
        Save( packet->UCID );
    }
}

void RCEnergy::InsimNPL( struct IS_NPL* packet )
{
    if (packet->NumP == 0)
        return;

    PLIDtoUCID[packet->PLID] = packet->UCID;
    players[packet->UCID].InSpec = false;
}

void RCEnergy::InsimPLP( struct IS_PLP* packet)
{
    players[PLIDtoUCID[packet->PLID]].Zone = 1;
    players[PLIDtoUCID[packet->PLID]].InSpec = true;

    //PLIDtoUCID.erase(packet->PLID);
}

void RCEnergy::InsimPLL( struct IS_PLL* packet )
{
    players[PLIDtoUCID[packet->PLID]].Zone = 1;
    players[PLIDtoUCID[packet->PLID]].InSpec = true;

    PLIDtoUCID.erase(packet->PLID);
}

void RCEnergy::InsimCNL( struct IS_CNL* packet )
{
    Save(packet->UCID);
    players.erase(packet->UCID);
}

void RCEnergy::Save (byte UCID)
{
    db->update("energy",
        {
            {"energy", ToString(players[UCID].Energy)},
            {"lock_to", ToString(players[UCID].LockTime)}
        },
        {
            {"username", players[UCID].UName}
        }
    );
}

void RCEnergy::InsimCPR( struct IS_CPR* packet )
{
    players[packet->UCID].PName = packet->PName;
}


void RCEnergy::InsimBTC( struct IS_BTC* packet )
{
    if (packet->ReqI == 55 && packet->ClickID >= 178 && packet->ClickID <= 182)
    {
        if (packet->ClickID == 178)
        {
            players[packet->UCID].Cost = 99;
            players[packet->UCID].Effect = 10;
        }
        if (packet->ClickID == 179)
        {
            players[packet->UCID].Cost = 199;
            players[packet->UCID].Effect = 20;
        }
        if (packet->ClickID == 180)
        {
            players[packet->UCID].Cost = 499;
            players[packet->UCID].Effect = 50;
        }
        if (packet->ClickID == 181)
        {
            players[packet->UCID].Cost = 749;
            players[packet->UCID].Effect = 75;
        }
        if (packet->ClickID == 182)
        {
            players[packet->UCID].Cost = 899;
            players[packet->UCID].Effect = 90;
        }

        insim->SendBFN(packet->UCID, 176, 182);
        players[packet->UCID].McDriveStep = 2;
        insim->SendMTC(packet->UCID, msg->_(packet->UCID, "MdChoose"));
    }
}

void RCEnergy::InsimMCI ( struct IS_MCI* pack_mci )
{
    for (int i = 0; i < pack_mci->NumC; i++)
    {
        byte UCID = PLIDtoUCID[pack_mci->Info[i].PLID];

        if (UCID == 0)
            continue;

        int X = pack_mci->Info[i].X / 65536;
        int Y = pack_mci->Info[i].Y / 65536;
        int Z = pack_mci->Info[i].Z / 65536;
        int D = pack_mci->Info[i].Direction / 182;
        int H = pack_mci->Info[i].Heading / 182;
        int Speed = (int)pack_mci->Info[i].Speed * 360 / 32768;

        bool bFound = false;

        if(config["cafe"].isArray()){
            for(auto cafe: config["cafe"]){
                if (CheckPosition(cafe["X"], cafe["Y"], X, Y))
                {
                    bFound = true;
                    break;
                }
            }
        }

        if(bFound){
            players[UCID].Zone = 3;
        }
        else if (players[UCID].Energy <= 0 && players[UCID].Zone != 1)
        {
            insim->SendMTC(UCID, msg->_(UCID, "EnergyOver"), SND_ERROR);
            players[UCID].Zone = 1;
            insim->SendMST("/spec " + players[UCID].UName);
        }
        else if (!players[UCID].InSpec)
        {
            players[UCID].Zone = 0;
        }

        //*************McDrive*********************************
        /**  ___________________________________________
            |   __________    __________    _________   |
            |  |          |  |          |  |         |  |
            |  |  choose  |  |   pay    |  |  take   |  |
            |  |          |  |          |  |         |  |
            |  |__________|  |__________|  |_________|  |
            |___________________________________________|
        **/

        if (CheckPosition(config["McDrive"]["Zone"]["X"], config["McDrive"]["Zone"]["Y"], X, Y))
        {
            // choose
            if (CheckPosition(config["McDrive"]["Choose"]["X"], config["McDrive"]["Choose"]["Y"], X, Y))
            {
                if (Speed == 0 && (players[UCID].McDriveStep == 0 || players[UCID].McDriveStep == 5))
                {
                    players[UCID].McDriveStep = 1;

                    insim->SendButton(255, UCID, 176, 165, 80, 34, 25, 16, "");
                    insim->SendButton(255, UCID, 177, 165, 80, 34, 4, 0, msg->_(UCID, "MdHeader"));

                    insim->SendButton(55, UCID, 178, 166, 84, 32, 4, 32 + 64 + 8, msg->_(UCID, "MdChose1"));
                    insim->SendButton(55, UCID, 179, 166, 88, 32, 4, 32 + 64 + 8, msg->_(UCID, "MdChose2"));
                    insim->SendButton(55, UCID, 180, 166, 92, 32, 4, 32 + 64 + 8, msg->_(UCID, "MdChose3"));
                    insim->SendButton(55, UCID, 181, 166, 96, 32, 4, 32 + 64 + 8, msg->_(UCID, "MdChose4"));
                    insim->SendButton(55, UCID, 182, 166, 100, 32, 4, 32 + 64 + 8, msg->_(UCID, "MdChose5"));
                }
                else if (Speed > 0 && players[UCID].McDriveStep == 1)
                {
                    players[UCID].McDriveStep = 0;
                    insim->SendBFN(UCID, 176, 182);
                }
            }

            // pay
            else if (CheckPosition(config["McDrive"]["Pay"]["X"], config["McDrive"]["Pay"]["Y"], X, Y) && Speed == 0)
            {
                if (players[UCID].McDriveStep == 0)
                {
                    players[UCID].McDriveStep = 5;
                    insim->SendMTC(UCID, msg->_(UCID, "MdNotChose"));
                    return;
                }

                if (players[UCID].McDriveStep == 2 || players[UCID].McDriveStep == 4)
                {
                    insim->SendBFN(UCID, 176, 182);

                    insim->SendMTC(UCID, StringFormat(msg->_(UCID, "MdPay"), players[UCID].Cost));

                    if (bank->GetCash(UCID) < players[UCID].Cost)
                    {
                        insim->SendMTC(UCID, msg->_(UCID, "2001" ));
                        players[UCID].McDriveStep = 0;
                        return;
                    }

                    players[UCID].McDriveStep = 3;

                    bank->RemCash(UCID, players[UCID].Cost);
                    bank->AddToBank(players[UCID].Cost);

                    players[UCID].Cost = 0;
                }
            }

            // take
            else if (CheckPosition(config["McDrive"]["Take"]["X"], config["McDrive"]["Take"]["Y"], X, Y) && Speed == 0)
            {
                if (players[UCID].McDriveStep == 2)
                {
                    players[UCID].McDriveStep = 4;
                    insim->SendMTC(UCID, msg->_(UCID, "MdNotPay"));
                    return;
                }

                if (players[UCID].McDriveStep == 3)
                {
                    insim->SendMTC(UCID, msg->_(UCID, "MdTake"));

                    players[UCID].leftToFeel = players[UCID].Effect;
                    players[UCID].feelStep = 1;
                    players[UCID].Effect = 0;
                    players[UCID].Cost = 0;

                    players[UCID].McDriveStep = 0;
                }
            }
        }
        else
        {
            if (players[UCID].McDriveStep != 0)
            {
                insim->SendBFN(UCID, 176, 182);
                players[UCID].McDriveStep = 0;
                players[UCID].Effect = 0;
                players[UCID].Cost = 0;
            }
            /*if (players[UCID].PayMcDrive == true)
            {
                insim->SendMTC(UCID, msg->_( UCID, "McDrive3.1" ));
                players[ UCID ].leftToFeel = players[UCID].Effect;
                players[ UCID ].feelStep = 1;
                players[UCID].Effect = 0;
                players[UCID].Cost = 0;
                players[UCID].PayMcDrive = false;
            }
            else*/
        }

//******************************************************

        int S = (int)pack_mci->Info[i].Speed * 360 / 32768;

        int A = pack_mci->Info[i].AngVel * 360 / 16384;

        int X1 = players[UCID].Info.X / 65536;
        int Y1 = players[UCID].Info.Y / 65536;
        int Z1 = players[UCID].Info.Z / 65536;
        int D1 = players[UCID].Info.Direction / 182;
        int H1 = players[UCID].Info.Heading / 182;
        int S1 = (int)players[UCID].Info.Speed * 360 / 32768;
        int A1 = players[UCID].Info.AngVel * 360 / 16384;

        float dA = A - A1;
        float dS = S - S1;
        float dD = fabs(sin(D) * 100.0f) - fabs(sin(D1) * 100.0f);
        float dH = fabs(sin(H) * 100.0f) - fabs(sin(H1) * 100.0f);

        float K = sqrt(fabs((dD - dH) * (1+dA) * dS)) / 800.0f;

        if (players[UCID].Energy > 0 && S > 5)
            if (!Islocked(UCID))
                RemoveEnergy(UCID, K);

        if (S == 0)
        {
            if(players[UCID].Energy < 100)
            {
                float toAdd = 0.01;
                if (players[UCID].Zone == 3)
                    toAdd = 0.02;

                if(players[UCID].Energy + toAdd > 100)
                    toAdd = 100 - players[UCID].Energy;

                AddEnergy(UCID, toAdd);
            }
        }

        if (X1 == 0 && Y1 == 0 && Z1 == 0)
        {
            X1=X;
            Y1=Y;
            Z1=Z;
        }

        memcpy( &players[UCID].Info , &pack_mci->Info[i] , sizeof(struct CompCar) );
    }
}


void RCEnergy::InsimMSO( struct IS_MSO* packet )
{
    if (packet->UCID == 0)
        return;

    xString Message = packet->Msg + packet->TextStart;
    vector<xString> args = Message.split(' ',1);

    if (Message == "!coffee")
    {
        if (bank->GetCash( packet->UCID ) < 50)
        {
            insim->SendMTC(packet->UCID, msg->_( packet->UCID, "2001" ));
            return;
        }

        if ( !( players[ packet->UCID ].Zone == 1 && players[ packet->UCID ].Energy < 5 ) && ( players[ packet->UCID ].Zone != 3 ) )
        {
            insim->SendMTC(packet->UCID, msg->_( packet->UCID, "2002" ));
            return;
        }

        if ( players[ packet->UCID ].Energy > 99 )
        {
            insim->SendMTC(packet->UCID, msg->_( packet->UCID, "EnergyFull" ));
            return;
        }

        if ( players[ packet->UCID ].leftToFeel > 0 )
        {
            insim->SendMTC(packet->UCID, msg->_( packet->UCID, "FillingEnergy" ));
            return;
        }

        players[ packet->UCID ].leftToFeel = 5;
        players[ packet->UCID ].feelStep = 0.5;

        bank->RemCash(packet->UCID, 50);
        bank->AddToBank(50);
    }

    //!redbule
    if (Message == "!redbull")
    {
        if (bank->GetCash( packet->UCID ) < 100)
        {
            insim->SendMTC(packet->UCID, msg->_( packet->UCID, "2001" ));
            return;
        }

        if ( !(players[packet->UCID].Zone == 1 && players[packet->UCID].Energy < 5) && (players[packet->UCID].Zone != 3 ) )
        {
            insim->SendMTC(packet->UCID, msg->_( packet->UCID, "2002" ));
            return;
        }

        if ( players[ packet->UCID ].Energy > 99 )
        {
            insim->SendMTC(packet->UCID, msg->_( packet->UCID, "EnergyFull" ));
            return;
        }

        if ( players[ packet->UCID ].leftToFeel > 0 )
        {
            insim->SendMTC(packet->UCID, msg->_( packet->UCID, "FillingEnergy" ));
            return;
        }

        players[ packet->UCID ].leftToFeel = 10;
        players[ packet->UCID ].feelStep = 1;
        bank->RemCash(packet->UCID, 100);
        bank->AddToBank(100);
    }
}


void RCEnergy::Event()
{
    for ( auto& play: players )
    {
        byte UCID = play.first;

        float nrg = players[UCID].Energy;
        int color = 2;

        if (nrg <= 10)
        {
            if (players[UCID].EnergyAlarm)
            {
                players[UCID].EnergyAlarm = false;
                color = 0;
            }
            else
            {
                players[UCID].EnergyAlarm = true;
                color = 1;
            }
        }
        else if (nrg <= 50)
            color = 3;

        string str = StringFormat(msg->_(UCID , "Energy"), color, nrg);

        if (players[UCID].Zone == 3)
            str = StringFormat(msg->_(UCID , "EnergyArrow"), str.c_str());

        insim->SendButton(255, UCID, 2, 100, 1, 30, 4, 32+64, str);

        if (players[UCID].leftToFeel > 0)
        {
            float step = players[UCID].feelStep;

            if(nrg < 100)
            {
                if(nrg + step > 100)
                    step = 100 - nrg;

                AddEnergy(UCID, step);
            }

            players[UCID].leftToFeel -= players[UCID].feelStep;
        }

        if(players[UCID].InSpec && nrg < 100)
            AddEnergy(UCID, 0.01f);

        if(players[UCID].LockTime < time(nullptr) && Islocked(UCID))
        {
            players[UCID].LockTime = 0;
            Unlock(UCID);
        }
        else if (players[UCID].LockTime > 0 && !Islocked(UCID))
            LockTo(UCID, players[UCID].LockTime);
    }
}

bool RCEnergy::InCafe( byte UCID )
{
    if (players[UCID].Zone == 3)
        return true;
    return false;
}


float RCEnergy::GetEnergy(byte UCID)
{
    return players[UCID].Energy;
}

void RCEnergy::InsimCON( struct IS_CON* packet )
{
    byte UCIDA = PLIDtoUCID[packet->A.PLID];
    byte UCIDB = PLIDtoUCID[packet->B.PLID];

    if (!Islocked(UCIDA))
    {
        RemoveEnergy(UCIDA, packet->SpClose / 10);
    }

    if (!Islocked(UCIDB))
    {
        RemoveEnergy(UCIDB, packet->SpClose / 10);
    }
}

void RCEnergy::InsimOBH( struct IS_OBH* packet )
{
    byte UCID = PLIDtoUCID[packet->PLID];
    if ((packet->Index > 45 && packet->Index < 125 && packet->Index != 120 && packet->Index != 121) || (packet->Index > 140))
    {
        time_t now = time(NULL);

        if ((now - players[UCID].LastT) < 1)
            return;

        players[UCID].LastT = now;

        if (!Islocked(UCID))
        {
            RemoveEnergy(UCID, packet->SpClose / 40);
        }
    }
}

void RCEnergy::InsimHLV( struct IS_HLV* packet )
{
    byte UCID = PLIDtoUCID[packet->PLID];

    if (packet->HLVC == 1)
    {
        time_t now = time(NULL);

        if ((now - players[UCID].LastT) < 1)
            return;

        players[UCID].LastT = now;

        if (!Islocked( UCID ))
        {
            RemoveEnergy(UCID, packet->C.Speed / 20);
        }
    }
}

bool RCEnergy::Lock(byte UCID)
{
    players[UCID].Lock = 1;
    return true;
}

bool RCEnergy::Unlock(byte UCID)
{
    players[UCID].Lock = 0;
    return true;
}

bool RCEnergy::Islocked(byte UCID)
{
    if (players[UCID].Lock == 1)
        return true;
    return false;
}

bool RCEnergy::AddEnergy( byte UCID, float Energy, bool feel)
{
    if (feel)
    {
        players[UCID].leftToFeel = Energy;
        players[UCID].feelStep = 1;
    }
    else
    {
        players[UCID].Energy += Energy;
    }

    if (players[UCID].Energy > MAX_ENERGY)
        players[UCID].Energy = MAX_ENERGY;

    return true;
}

bool RCEnergy::RemoveEnergy(byte UCID, float Energy)
{
    players[UCID].Energy -= Energy;

    if(players[UCID].Energy < 0)
        players[UCID].Energy = 0;

    return true;
}

bool
RCEnergy::LockTo(byte UCID, time_t LockTime)
{
    if(LockTime < time(nullptr))
        return false;

    players[UCID].LockTime = LockTime;
    this->Lock(UCID);

    return true;
}

void
RCEnergy::SaveAll()
{
    for( auto i: players)
    {
        Save(i.first);
    }
}
