using namespace std;
#include "RCQuest.h"

RCQuest*
RCQuest::self = nullptr;

RCQuest*
RCQuest::getInstance()
{
    if(!self)
        self = new RCQuest();

    return self;
}

RCQuest::RCQuest()
{
    ClassName = "RCQuest";
}

RCQuest::~RCQuest()
{

}

int RCQuest::init(const char* Dir)
{
    strcpy(RootDir,Dir);

    msg = RCMessage::getInstance();
    if (!msg)
    {
        printf ("^3RCEnergy:\t^1Can't struct RCMessage class");
        return 0;
    }

    bank = RCBank::getInstance();
    if (!msg)
    {
        printf ("^3RCEnergy:\t^1Can't struct RCBank class");
        return 0;
    }

    this->db = DBMySQL::Get();
    if (!this->db)
    {
        CCText("^3"+ClassName+": Can't sctruct MySQL Connector\n");
        return 0;
    }

    insim = CInsim::getInstance();
    if (!insim)
    {
        CCText ("^3"+ClassName+": Can't struct CInsim class");
        return 0;
    }

    CCText("^3"+ClassName+":\t\t^2inited");

    return 1;
}

void RCQuest::ReadConfig(const char *Track)
{
    CCText("  ^7"+ClassName+"\t^3OFF");
    return;

    char file[MAX_PATH];
    sprintf(file, "%s/%s/%s.json", RootDir, ClassName.c_str(), Track);

    ifstream readf (file, ios::in);

    if(!readf.is_open())
    {
        CCText("  ^7"+ClassName+"    ^1ERROR\n^8(File " + (string)file + " not found)");
        return ;
    }

    bool readed = configReader.parse( readf, config, false );

    if ( !readed )
    {
        readf.close();
        // report to the user the failure and their locations in the document.
        cout  << ClassName+": Failed to parse configuration\n"
                   << configReader.getFormattedErrorMessages();
        return;
    }
    readf.close();

    if(!config.isObject ())
    {
        CCText("  ^7"+ClassName+"\t^1FAIL");
        return;
    }

    CCText("  ^7"+ClassName+"\t^2OK");
}


// обработчик события когда на сервер заходит новый пользователь
void RCQuest::InsimNCN( struct IS_NCN *packet )
{
    if ( packet->UCID == 0 )
        return;

    players[ packet->UCID ].UName = packet->UName;
    players[ packet->UCID ].PName = packet->PName;
    players[packet->UCID].Admin = packet->Admin;

    if(!packet->Admin)
    {
        players[packet->UCID].Admin = this->isAdmin(packet->UName);
    }
}

void RCQuest::InsimNPL( struct IS_NPL *packet )
{
    PLIDtoUCID[packet->PLID] = packet->UCID;

    if (players[packet->UCID].quest["step"] > 0)
        players[packet->UCID].quest["AddedMass"] = packet->H_Mass;
    else
        players[packet->UCID].quest["AddedMass"] = 0;
}

void RCQuest::InsimPLP( struct IS_PLP *packet )
{
    PLIDtoUCID.erase( packet->PLID );
}

void RCQuest::InsimPLL( struct IS_PLL *packet )
{
    PLIDtoUCID.erase( packet->PLID );
}

void RCQuest::InsimCNL( struct IS_CNL *packet )
{
    players.erase( packet->UCID );
}

void RCQuest::InsimCPR( struct IS_CPR *packet )
{
    players[ packet->UCID ].PName = packet->PName;
}

void RCQuest::InsimMSO( struct IS_MSO *packet )
{
    byte UCID = packet->UCID;

    if ( UCID == 0 )
        return;

    if ( packet->UserType != MSO_PREFIX )
        return;

    string Msg = packet->Msg + packet->TextStart;

    if (Msg=="!go" && players[UCID].quest["step"] == 1)
    {
        players[UCID].quest["step"] = 2;                                               //Переход сразу на 6 шаг

        insim->SendMTC(UCID, msg->_( UCID, "QuestStep2.1" ));
        insim->SendMTC(UCID, msg->_( UCID, "QuestStep2.2" ));
    }

    if (Msg=="!end" && players[UCID].quest["step"] == 7)
    {

        insim->SendMTC(UCID, msg->_( UCID, "QuestStep7.01" ));
        bank->AddCash(UCID, 20000, true);

        players[UCID].quest["step"] = 0;
        players[UCID].quest["stopTime"] = 0;
        players[UCID].quest["AddedMass"] = 0;
    }
}

void RCQuest::InsimMCI(struct IS_MCI* pack_mci)
{

    for (int i = 0; i < pack_mci->NumC; i++)
    {
        byte UCID = PLIDtoUCID[ pack_mci->Info[i].PLID ];
        int X = pack_mci->Info[i].X / 65536;
        int Y = pack_mci->Info[i].Y / 65536;
        int Speed = ((int)pack_mci->Info[i].Speed * 360) / (32768);

        if(Speed < 5)
            players[UCID].quest["stopTime"] = players[UCID].quest["stopTime"].asInt() + 1;
        else
            players[UCID].quest["stopTime"] = 0;

        if (Distance(X,Y,-519,245) < 10 && Speed<10 && players[UCID].quest["step"]==0)                          //Остановка
        {
            players[UCID].InBus = true;

            if (players[UCID].quest["stopTime"] == 40)
            {
                players[UCID].quest["step"] = 1;
                players[UCID].quest["stopTime"] = 0;
                insim->SendMTC(UCID, msg->_( UCID, "QuestStep1.1" ));
                insim->SendMTC(UCID, msg->_( UCID, "QuestStep1.2" ));
                insim->SendMTC(UCID, msg->_( UCID, "QuestStep1.3" ));
            }
        }

        if (players[UCID].quest["step"] == 2 && Distance(X,Y,413,-566) < 10 && Speed < 5)                               //Аэропорт
        {
            players[UCID].quest["step"] = 3;
            players[UCID].quest["stopTime"] = 0;
            insim->SendMTC(UCID, msg->_( UCID, "QuestStep3.1" ));
            insim->SendMTC(UCID, msg->_( UCID, "QuestStep3.2" ));
            insim->SendMTC(UCID, msg->_( UCID, "QuestStep3.3" ));
        }

        if (players[UCID].quest["step"] == 3 && Speed < 5 && Distance(X,Y,-373,507) < 10)                               //Друг
        {
            players[UCID].quest["step"] = 4;
            players[UCID].quest["stopTime"] = 0;
            insim->SendMTC(UCID, msg->_( UCID, "QuestStep4.1" ));
            insim->SendMTC(UCID, msg->_( UCID, "QuestStep4.2" ));
            insim->SendMTC(UCID, msg->_( UCID, "QuestStep4.3" ));
            insim->SendMTC(UCID, msg->_( UCID, "QuestStep4.4" ));

        }

        if (players[UCID].quest["step"] == 4 && Speed < 5 && Distance(X,Y,15,-44) < 10)                                //Магазин
        {
            bank->AddCash(UCID, 2000, true);
            players[UCID].quest["step"] = 5;
            players[UCID].quest["stopTime"] = 0;

            insim->SendMTC(UCID, msg->_( UCID, "QuestStep5.1" ));
            insim->SendMTC(UCID, msg->_( UCID, "QuestStep5.2" ));
            insim->SendMTC(UCID, msg->_( UCID, "QuestStep5.3" ));
        }

        if (players[UCID].quest["step"] == 5 && Speed < 5 && Distance(X,Y,235,15) < 10 )                                //Встреча с механиком
        {
            players[UCID].quest["step"] = 6;
            players[UCID].quest["stopTime"] = 0;

            insim->SendMTC(UCID, msg->_( UCID, "QuestStep6.1" ));
            insim->SendMTC(UCID, msg->_( UCID, "QuestStep6.2" ));
            insim->SendMTC(UCID, msg->_( UCID, "QuestStep6.3" ));
        }

        if (players[UCID].quest["step"] == 6 && Speed < 5 && Distance(X,Y,15,-44) < 10)                                //Ограбленный магазин
        {
            if (players[UCID].quest["AddedMass"] > 190)
            {
                players[UCID].quest["step"] = 7;

                insim->SendMTC(UCID, msg->_( UCID, "QuestStep7.1" ));
                insim->SendMTC(UCID, msg->_( UCID, "QuestStep7.2" ));
                insim->SendMTC(UCID, msg->_( UCID, "QuestStep7.3" ));
                insim->SendMTC(UCID, msg->_( UCID, "QuestStep7.4" ));
                insim->SendMTC(UCID, msg->_( UCID, "QuestStep7.5" ));
            }
            else
            {
                insim->SendMTC(UCID, msg->_( UCID, "QuestStep7.6" ));
                players[UCID].quest["step"] = 5;
            }
        }

        if (players[UCID].quest["step"] == 7 && Speed < 5 && Distance(X,Y,-377,354) < 10)                                //Полицейский участок
        {
            players[UCID].quest["step"] = 8;
            players[UCID].quest["stopTime"] = 0;

            insim->SendMTC(UCID, msg->_( UCID, "QuestStep7.1" ));
            insim->SendMTC(UCID, msg->_( UCID, "QuestStep7.2" ));
            insim->SendMTC(UCID, msg->_( UCID, "QuestStep7.3" ));
            insim->SendMTC(UCID, msg->_( UCID, "QuestStep7.4" ));
            insim->SendMTC(UCID, msg->_( UCID, "QuestStep7.5" ));
        }

/*        char Text[64];
        sprintf(Text, "X = %d", X);
        insim->SendMTC(UCID, Text);
        sprintf(Text, "Y = %d", Y);
        insim->SendMTC(UCID, Text);
        sprintf(Text, "Z = %d", Z);
        insim->SendMTC(UCID, Text);
        sprintf(Text, "Step = %d", QQuest.quest["step"]);
        insim->SendMTC(UCID, Text);
        sprintf(Text, "Time = %d", QQuestquest["stopTime"]);
        insim->SendMTC(UCID, Text);
        sprintf(Text, "AddMass = %d", QQuest.quest["AddedMass"]);
        insim->SendMTC(UCID, Text);  */

    }

}

void RCQuest::Event()
{
    //Евент
}
